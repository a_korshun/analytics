from costs_af_nebula_ios import appsflyer_cost
from costs_af_nebula_android import appsflyer_cost_android
from costs_additional import additional_costs
from costs_af_zodi_ios import appsflyer_cost_zodi

from installs_af_nebula_ios import appsflyer_installs
from installs_organic_af_nebula_ios import appsflyer_organic_installs
from installs_af_nebula_android import appsflyer_installs_android
from installs_organic_af_nebula_android import appsflyer_organic_installs_android
from installs_af_zodi_ios import appsflyer_installs_zodi
from installs_organic_af_zodi_ios import appsflyer_organic_installs_zodi

from inapps_af_nebula_ios import appsflyer_inapps
from inapps_af_zodi_ios import appsflyer_inapps_zodi
from inapps_af_nebula_android import appsflyer_inapps_android
from payments_mysql_nebula_ios import mysql_payments
from payments_mysql_zodi_ios import mysql_payments_zodi
from payments_mysql_nebula_android import mysql_payments_android

from deduplication import dedup
from backup import backup_bq, clear_bq_backups
from firebase_to_gcs import firebase_to_gcs

from helpers.messaging import msg_to_slack_me, format_slack_attachment

from adrevenue_ironsrc_imprlevel import ironsrc_revenue_impressionlevel 
from adrevenue_ironsrc_userlevel import ironsrc_revenue_userlevel
from adrevenue_appodeal import appodeal_revenue
from adrevenue_ironsrc import ironsrc_revenue

from launches_af_nebula_android import launches_extract_android
from launches_af_nebula_ios import launches_extract_ios
from launches_af_zodi_ios import launches_extract_ios_zodi

import pandas as pd
from datetime import datetime

def appodeal_revenue_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppodealRevenue"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appodeal_revenue()

    attachment = format_slack_attachment(
        gcf_function,
        appodeal_revenue.target_table,
        "*Inserted:*\n {}".format(str(appodeal_revenue.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def ironsrc_revenue_import(event, context):
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-IronscrRevenue"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    ironsrc_revenue()

    attachment = format_slack_attachment(
        gcf_function,
        ironsrc_revenue.target_table,
        "*Inserted:*\n {}".format(str(ironsrc_revenue.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def ironsrc_revenue_userlevel_import(event, context):
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-IronsrcRevenueUserLevel"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    ironsrc_revenue_userlevel()

    attachment = format_slack_attachment(
        gcf_function,
        ironsrc_revenue_userlevel.target_table,
        "*Inserted:*\n {}".format(str(ironsrc_revenue_userlevel.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def ironsrc_revenue_impressionlevel_import(event, context):
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-IronsrcRevenueImpressionLevel"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    ironsrc_revenue_impressionlevel()

    attachment = format_slack_attachment(
        gcf_function,
        ironsrc_revenue_impressionlevel.target_table,
        "*Inserted:*\n {}".format(
            str(ironsrc_revenue_impressionlevel.report_size) + " rows"
        ),
    )
    msg_to_slack_me(message="", attachment=attachment)

def appsflyer_cost_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerCosts"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_cost()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_cost.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_cost.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def appsflyer_cost_import_zodi(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerCostsZodi"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_cost_zodi()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_cost_zodi.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_cost_zodi.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)

def appsflyer_cost_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerCostAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_cost_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_cost_android.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_cost_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def doc_additional_costs_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AdditionalCostsImport"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )
    
    additional_costs()

    attachment = format_slack_attachment(
        gcf_function,
        additional_costs.target_table,
        "*Inserted:*\n {}".format(str(additional_costs.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def appsflyer_installs_organic_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsOrganic"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_organic_installs()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_organic_installs.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_organic_installs.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def appsflyer_installs_organic_zodi_import(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsOrganicZodi"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_organic_installs_zodi()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_organic_installs_zodi.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_organic_installs_zodi.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_installs_organic_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsOrganicAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_organic_installs_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_organic_installs_android.target_table,
        "*Inserted:*\n {}".format(
            str(appsflyer_organic_installs_android.report_size) + " rows"
        ),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_installs_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstalls"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_installs()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_installs.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_installs.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def appsflyer_installs_import_zodi(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsZodi"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_installs_zodi()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_installs_zodi.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_installs_zodi.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_installs_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_installs_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_installs_android.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_installs_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def mysql_payments_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-MySqlPayments"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    mysql_payments()

    attachment = format_slack_attachment(
        gcf_function,
        mysql_payments.target_table,
        "*Inserted:*\n {}".format(str(mysql_payments.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def mysql_payments_zodi_import(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-MySqlPaymentsZodi"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    mysql_payments_zodi()

    attachment = format_slack_attachment(
        gcf_function,
        mysql_payments_zodi.target_table,
        "*Inserted:*\n {}".format(str(mysql_payments_zodi.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def mysql_payments_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-MysqlPaymentsAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    mysql_payments_android()

    attachment = format_slack_attachment(
        gcf_function,
        mysql_payments_android.target_table,
        "*Inserted:*\n {}".format(str(mysql_payments_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_inapps_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInapps"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_inapps()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_inapps.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_inapps.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)

def appsflyer_inapps_zodi_import(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInappsZodi"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_inapps_zodi()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_inapps_zodi.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_inapps_zodi.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)

def appsflyer_inapps_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInappsAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_inapps_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_inapps_android.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_inapps_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def appsflyer_launches_android(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerLaunchesAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    launches_extract_android()

    attachment = format_slack_attachment(
        gcf_function,
        launches_extract_android.target_table,
        "*Inserted:*\n {}".format(str(launches_extract_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)

def appsflyer_launches_ios(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerLaunchesIos"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    launches_extract_ios()

    attachment = format_slack_attachment(
        gcf_function,
        launches_extract_ios.target_table,
        "*Inserted:*\n {}".format(str(launches_extract_ios.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def appsflyer_launches_ios_zodi(event, context):
    
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerLaunchesIosZodi"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    launches_extract_ios_zodi()

    attachment = format_slack_attachment(
        gcf_function,
        launches_extract_ios_zodi.target_table,
        "*Inserted:*\n {}".format(str(launches_extract_ios_zodi.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)

def deduplicate(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-Deduplicate"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    dedup()

    message = """*Removed:* \n DimUser -> {0} \n FactTransaction -> {1} \n FactAdRevenue -> {2} \n FactMarketingSpent -> {3}""".format(
        dedup.du_removed, dedup.ft_removed, dedup.fad_removed, dedup.fm_removed
    )

    attachment = format_slack_attachment(gcf_function, "Fact Tables", message)
    msg_to_slack_me(message="", attachment=attachment)


def backup_bigquery(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-BackUpBQ"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    backup_bq()

    message = """*Backed Up:*\n {0} tables to GCS""".format(str(backup_bq.tables_num))

    attachment = format_slack_attachment(gcf_function, "DWH", message)
    msg_to_slack_me(message="", attachment=attachment)


def remove_expired_backups(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-ClearBackUps"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    clear_bq_backups()

    message = """*Removed:*\n{} files from GCS""".format(str(clear_bq_backups.blobs_size))

    attachment = format_slack_attachment(gcf_function, "Cloud Storage", message)

    msg_to_slack_me(message="", attachment=attachment)
    
def firebase_bq_export(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-FirebaseBqExport"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    firebase_to_gcs()

    message = """*Exported:*\n{} tables(s) to GCS""".format(str(firebase_to_gcs.tables_num))

    attachment = format_slack_attachment(gcf_function, "Cloud Storage", message)

    msg_to_slack_me(message="", attachment=attachment)