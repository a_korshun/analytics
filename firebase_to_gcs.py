import os
import time
from datetime import datetime, timedelta
from time import gmtime, strftime

from google.cloud import bigquery 

from helpers.data_etl import bq_to_gcs

key = "key.json"
project = "nebula-horoscope"
dataset_id = "analytics_198124292"
gs_folder = "Firebase-Export"
gs_bucket = "bq-etl-nebula"
client = bigquery.Client.from_service_account_json(key)
credentials = client

def firebase_to_gcs():
    
    tables = client.list_tables(dataset_id)
    tables_list = []
    for table in tables:
        if table.table_id[-8:] <= (datetime.now() - timedelta(days=30)).strftime("%Y%m%d"):
            bq_to_gcs(
                project=project,
                dataset_id=dataset_id,
                gs_folder=gs_folder,
                gs_bucket=gs_bucket,
                table=table,
                destination_format="json",
                compression="gzip",
                delete_bq_table=True,
            )
            tables_list.append(table.table_id)
    
    firebase_to_gcs.tables_num = len(tables_list)