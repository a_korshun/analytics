def appsflyer_organic_installs_android():
    
    from sqlalchemy import create_engine
    import pymysql
    import pandas as pd
    import numpy as np
    import gcsfs
    from dask.diagnostics import ProgressBar
    from dask import dataframe as dd
    from google.cloud import bigquery, storage
    from time import gmtime, strftime, sleep
    from datetime import datetime, timedelta

    from tqdm import tqdm

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql
    from clients.appsflyer import AppsFlyer

    import warnings
    warnings.filterwarnings("ignore")
    
    primary_token = "29d85f91-d29b-4b43-a62d-e65b0633efef"
    app_id = "genesis.nebula"
    af = AppsFlyer(api_token=primary_token, app_id=app_id)
    
    installs_organic = pd.DataFrame()
    for d in tqdm(
        pd.date_range(
            (datetime.now().date() - timedelta(days=2)).isoformat(),
            (datetime.now().date()).isoformat(),
            freq="1D",
        ).tolist()
    ):
        resp_organic = af.organic_installs_report(
            date_from=d.strftime("%Y-%m-%d"),
            date_to=d.strftime("%Y-%m-%d"),
            as_df=True,
        )

        installs_organic = pd.concat([installs_organic, resp_organic])
        sleep(1)
    
    installs_organic["UserClientId"] = np.nan
    dimuser = installs_organic[
        [
            "UserClientId",
            "AppsFlyer ID",
            "Attributed Touch Type",
            "Attributed Touch Time",
            "Install Time",
            "Partner",
            "Media Source",
            "Channel",
            "Campaign",
            "Campaign ID",
            "Adset",
            "Adset ID",
            "Ad",
            "Ad ID",
            "Country Code",
            "IP",
            "WIFI",
            "Language",
            "Advertising ID",
            "IDFV",
            "Platform",
            "Device Type",
            "OS Version",
            "App Version",
            "App Name",
            "Is Retargeting",
        ]
    ].copy()
    
    dimuser.rename(
        columns={
            "AppsFlyer ID": "UserAppsflyerId",
            "Attributed Touch Type": "AttributionEventTypeId",
            "Attributed Touch Time": "AttributionEventTime",
            "Install Time": "InstallTime",
            "Partner": "AgencyId",
            "Media Source": "MediaSourceId",
            "Channel": "ChannelId",
            "Campaign": "CampaignNameShort",
            "Campaign ID": "CampaignId",
            "Adset": "AdSetNameShort",
            "Adset ID": "AdSetId",
            "Ad": "AdNameShort",
            "Ad ID": "AdId",
            "Country Code": "CountryId",
            "IP": "RegistrationIp",
            "Advertising ID": "IDFA",
            "WIFI": "IsWifiRegistration",
            "Language": "LanguageId",
            "Platform": "PlatformId",
            "Device Type": "DeviceId",
            "OS Version": "OsVersion",
            "App Version": "AppVersion",
            "App Name": "AppInternalId",
            "Is Retargeting": "IsRetargeting",
        },
        inplace=True,
    )
    
    obj_columns = [
        "UserAppsflyerId",
        "AttributionEventTypeId",
        "AgencyId",
        "MediaSourceId",
        "ChannelId",
        "CampaignNameShort",
        "CampaignId",
        "AdSetId",
        "AdId",
        "AdSetNameShort",
        "AdNameShort",
        "CountryId",
        "LanguageId",
        "PlatformId",
        "DeviceId",
        "AppInternalId",
    ]

    for col in obj_columns:
        dimuser[col] = (
            dimuser[col]
            .replace([0, -1, "None", np.nan, " "], "Unknown")
            .fillna("Unknown")
            .apply(lambda x: str(x).rstrip(" "))
        )

    numeric_columns = ["CampaignId", "AdSetId", "AdId"]

    for c in numeric_columns:
        dimuser[c] = dimuser[c].apply(lambda x: "id" + str(x) if x != "Unknown" else x)

    dim_columns = [
        ["CampaignInternalId", "CampaignId", "CampaignNameShort"],
        ["AdSetInternalId", "AdSetId", "AdSetNameShort"],
        ["AdInternalId", "AdId", "AdNameShort"],
    ]

    for c in dim_columns:
        dimuser[c[0]] = (
            dimuser[c[1]].astype(str) + " - " + dimuser[c[2]].astype(str)
        ).replace("Unknown - Unknown", "Unknown")

    other_columns = np.setdiff1d(
        dimuser.columns.tolist(),
        obj_columns + ["AdInternalId", "AdSetInternalId", "CampaignInternalId"],
    ).tolist()

    for col in other_columns:
        dimuser[col] = dimuser[col].replace(
            [0, -1, "None", "Unknown", np.nan, " ", ""], np.nan
        )

    dimuser["CountryId"] = dimuser["CountryId"].replace(["UK", "AN"], "GB")
    dimuser["CountryId"] = dimuser["CountryId"].replace(
        ["NA", "No", "EU", " ", "", "XX", "ZZ"], "Unknown"
    )

    dimuser["UserTypeId"] = "Organic"
    dimuser["AppInternalId"] = 0
    dimuser["OsVersion"] = "v." + dimuser["OsVersion"].astype(str)
    dimuser["AppVersion"] = dimuser["AppVersion"].apply(lambda x: str(x)+'.0' if len(str(x))<=3 else x)
    
    update_dim(
        data_df=dimuser,
        dim="DimDevice",
        cat_column_df="DeviceId",
        cat_column_dim="DeviceName",
        id_column_dim="DeviceId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimPlatform",
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimLanguage",
        cat_column_df="LanguageId",
        cat_column_dim="LanguageName",
        id_column_dim="LanguageId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimUserType",
        cat_column_df="UserTypeId",
        cat_column_dim="UserTypeName",
        id_column_dim="UserTypeId",
    )
    
    map_dim(
        dim="DimDevice",
        data_df=dimuser,
        cat_column_df="DeviceId",
        cat_column_dim="DeviceName",
        id_column_dim="DeviceId",
    )

    map_dim(
        dim="DimPlatform",
        data_df=dimuser,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )

    map_dim(
        dim="DimLanguage",
        data_df=dimuser,
        cat_column_df="LanguageId",
        cat_column_dim="LanguageName",
        id_column_dim="LanguageId",
    )

    map_dim(
        dim="DimCountry",
        data_df=dimuser,
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )

    map_dim(
        dim="DimAd",
        data_df=dimuser,
        cat_column_df="AdInternalId",
        cat_column_dim="AdName",
        id_column_dim="AdInternalId",
    )

    map_dim(
        dim="DimAdSet",
        data_df=dimuser,
        cat_column_df="AdSetInternalId",
        cat_column_dim="AdSetName",
        id_column_dim="AdSetInternalId",
    )

    map_dim(
        dim="DimCampaign",
        data_df=dimuser,
        cat_column_df="CampaignInternalId",
        cat_column_dim="CampaignName",
        id_column_dim="CampaignInternalId",
    )

    map_dim(
        dim="DimChannel",
        data_df=dimuser,
        cat_column_df="ChannelId",
        cat_column_dim="ChannelName",
        id_column_dim="ChannelId",
    )

    map_dim(
        dim="DimMediaSource",
        data_df=dimuser,
        cat_column_df="MediaSourceId",
        cat_column_dim="MediaSourceName",
        id_column_dim="MediaSourceId",
    )

    map_dim(
        dim="DimAttributionEventType",
        data_df=dimuser,
        cat_column_df="AttributionEventTypeId",
        cat_column_dim="AttributionEventTypeName",
        id_column_dim="AttributionEventTypeId",
    )

    map_dim(
        dim="DimAgency",
        data_df=dimuser,
        cat_column_df="AgencyId",
        cat_column_dim="AgencyName",
        id_column_dim="AgencyId",
    )

    map_dim(
        data_df=dimuser,
        dim="DimUserType",
        cat_column_df="UserTypeId",
        cat_column_dim="UserTypeName",
        id_column_dim="UserTypeId",
    )
    
    dimuser["IsRetargeting"] = dimuser["IsRetargeting"].astype(bool)
    dimuser["IsWifiRegistration"] = dimuser["IsWifiRegistration"].astype(bool)

    dimuser["InstallTime"] = pd.to_datetime(dimuser["InstallTime"])
    dimuser["AttributionEventTime"] = pd.to_datetime(dimuser["AttributionEventTime"])

    dimuser.drop(
        columns=[
            "CampaignNameShort",
            "AdSetNameShort",
            "AdNameShort",
            "AdSetId",
            "AdId",
            "CampaignId",
        ],
        inplace=True,
    )
    
    dimuser = dimuser[
        [
            "UserClientId",
            "UserAppsflyerId",
            "AttributionEventTime",
            "AttributionEventTypeId",
            "InstallTime",
            "AgencyId",
            "MediaSourceId",
            "ChannelId",
            "CampaignInternalId",
            "AdSetInternalId",
            "AdInternalId",
            "CountryId",
            "RegistrationIp",
            "IsWifiRegistration",
            "LanguageId",
            "IDFA",
            "IDFV",
            "PlatformId",
            "DeviceId",
            "OsVersion",
            "AppVersion",
            "AppInternalId",
            "IsRetargeting",
            "UserTypeId",
        ]
    ]
    
    appsflyer_organic_installs_android.report_size = len(dimuser)
    
    appsflyer_organic_installs_android.target_table = 'DimUser'

    df_to_bq(
        dimuser,
        table=appsflyer_organic_installs_android.target_table,
        schema=[
            bigquery.SchemaField("UserClientId", "INTEGER"),
            bigquery.SchemaField("UserAppsflyerId", "STRING"),
            bigquery.SchemaField("AttributionEventTime", "TIMESTAMP"),
            bigquery.SchemaField("AttributionEventTypeId", "INTEGER"),
            bigquery.SchemaField("InstallTime", "TIMESTAMP"),
            bigquery.SchemaField("AgencyId", "INTEGER"),
            bigquery.SchemaField("MediaSourceId", "INTEGER"),
            bigquery.SchemaField("ChannelId", "INTEGER"),
            bigquery.SchemaField("CampaignInternalId", "INTEGER"),
            # bigquery.SchemaField("CampaignId", "STRING"),
            bigquery.SchemaField("AdSetInternalId", "INTEGER"),
            # bigquery.SchemaField("AdSetId", "STRING"),
            bigquery.SchemaField("AdInternalId", "INTEGER"),
            # bigquery.SchemaField("AdId", "STRING"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("RegistrationIp", "STRING"),
            bigquery.SchemaField("IsWifiRegistration", "BOOLEAN"),
            bigquery.SchemaField("LanguageId", "INTEGER"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("DeviceId", "INTEGER"),
            bigquery.SchemaField("OsVersion", "STRING"),
            bigquery.SchemaField("AppVersion", "STRING"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("IsRetargeting", "BOOLEAN"),
            bigquery.SchemaField("UserTypeId", "INTEGER"),
        ],
        overwrite=False,
    )