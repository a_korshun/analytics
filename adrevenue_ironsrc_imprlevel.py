def ironsrc_revenue_impressionlevel():
    
    import pandas as pd
    import numpy as np
    from tqdm import tqdm
    from datetime import datetime, timedelta
    from clients.ironsource import Ironsrc
    from google.cloud import bigquery, storage

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, dml_for_bq

    secretkey = "6dc299817f5836174e69d9a5e26d9fca"
    refreshToken = "9691bb13dd7646d59ec7bd6fa1f58ac6"

    appKeys = {
        "Nebula Android": "cc242c8d",
        "Nebula iOS": "cc2d6355",
        "Zodi iOS": "d8fde7c1",
        "Party Game":'e1918811'
    }
    appPlatform = {"cc242c8d": 2, "cc2d6355": 0, "d8fde7c1": 0, 'e1918811':0}

    irnsrc = Ironsrc(secretkey, refreshToken)

    impr_level_report = pd.DataFrame()
    dates = pd.date_range(
        (datetime.now().date() - timedelta(days=3)).isoformat(),
        (datetime.now().date() - timedelta(days=1)).isoformat(),
        freq="1D",
    ).tolist()
    for d in tqdm(dates):
        for app in appKeys.values():
            try:
                report_chunk = irnsrc.detailed_report(
                    date=d.date(), report_type="impression_level", appKey=app
                )
                report_chunk["IronsrcAppId"] = app
                impr_level_report = pd.concat([impr_level_report, report_chunk])
            except Exception as e:
                print(e)
                pass 
            
    conditions_idfa = [
        impr_level_report.advertising_id_type == "idfa",
        impr_level_report.advertising_id_type == "gaid",
    ]

    conditions_idfv = [
        impr_level_report.advertising_id_type == "idfv",
    ]

    choices = [impr_level_report["advertising_id"], impr_level_report["advertising_id"]]

    impr_level_report["IDFA"] = np.select(conditions_idfa, choices, default=np.nan)
    impr_level_report["IDFV"] = np.select(conditions_idfv, [choices[0]], default=np.nan)
    impr_level_report.drop(columns=["advertising_id", "advertising_id_type"], inplace=True)

    impr_level_report["PlatformId"] = impr_level_report["IronsrcAppId"].map(appPlatform)

    app_map = {
        "cc242c8d":0,
        "cc2d6355":0,
        "d8fde7c1":4,
        "e1918811":5,
    }

    impr_level_report['AppInternalId'] = impr_level_report["IronsrcAppId"].map(app_map)
    
    impr_level_report.rename(
        columns={
            "ad_unit": "InAppAdTypeId",
            "user_id": "UserId",
            "placement": "InAppAdPlacementId",
            "ad_network": "InAppAdNetworkId",
            "segment": "InAppAdSegmentId",
            "instance_name": "InAppAdInstanceId",
            "impressions": "Impressions",
            "revenue": "Revenue",
            "AB_Testing": "AbTestGroup",
            "event_timestamp": "Time",
            "country": "CountryId",
        },
        inplace=True,
    )

    impr_level_report = impr_level_report[
        [
            "Time",
            "UserId",
            "IDFA",
            "IDFV",
            "PlatformId",
            "IronsrcAppId",
            "CountryId",
            "InAppAdSegmentId",
            "InAppAdTypeId",
            "InAppAdPlacementId",
            "InAppAdNetworkId",
            "InAppAdInstanceId",
            "AbTestGroup",
            "Revenue",           
            "AppInternalId"
        ]
    ]

    object_columns = [
        "CountryId",
        "InAppAdSegmentId",
        "InAppAdTypeId",
        "InAppAdPlacementId",
        "InAppAdNetworkId",
        "InAppAdInstanceId",
        "AbTestGroup",
    ]

    for col in object_columns:
        impr_level_report[col] = impr_level_report[col].replace(
            [0, -1, "None", np.nan], "Unknown"
        )

    numeric_columns = [
        "Revenue",
    ]

    for col in numeric_columns:
        impr_level_report[col] = impr_level_report[col].replace(
            [0, -1, "None", np.nan, "Unknown"], 0
        )

    impr_level_report["CountryId"] = impr_level_report["CountryId"].replace(
        ["UK", "AN"], "GB"
    )
    impr_level_report["CountryId"] = impr_level_report["CountryId"].replace(
        ["NA", "No", "EU", " ", "", "XX", "ZZ"], "Unknown"
    )

    update_dim(
        data_df=impr_level_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    map_dim(
        data_df=impr_level_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    update_dim(
        data_df=impr_level_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )

    map_dim(
        data_df=impr_level_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )

    update_dim(
        data_df=impr_level_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )

    map_dim(
        data_df=impr_level_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )

    update_dim(
        data_df=impr_level_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )

    map_dim(
        data_df=impr_level_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )

    update_dim(
        data_df=impr_level_report,
        dim="DimInAppAdInstance",
        cat_column_df="InAppAdInstanceId",
        cat_column_dim="InAppAdInstanceName",
        id_column_dim="InAppAdInstanceId",
    )

    map_dim(
        data_df=impr_level_report,
        dim="DimInAppAdInstance",
        cat_column_df="InAppAdInstanceId",
        cat_column_dim="InAppAdInstanceName",
        id_column_dim="InAppAdInstanceId",
    )

    map_dim(
        data_df=impr_level_report,
        dim="DimCountry",
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )

    impr_level_report["Time"] = pd.to_datetime(impr_level_report["Time"])

    ironsrc_revenue_impressionlevel.report_size = len(impr_level_report)
    
    ironsrc_revenue_impressionlevel.target_table = 'FactAdRevenueImpressionLevel'

    df_to_bq(
        impr_level_report,
        table=ironsrc_revenue_impressionlevel.target_table,
        schema=[
            bigquery.SchemaField("Time", "TIMESTAMP"),
            bigquery.SchemaField("UserId", "STRING"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("InAppAdSegmentId", "INTEGER"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("IronsrcAppId", "STRING"),
            bigquery.SchemaField("InAppAdTypeId", "INTEGER"),
            bigquery.SchemaField("InAppAdPlacementId", "INTEGER"),
            bigquery.SchemaField("InAppAdNetworkId", "INTEGER"),
            bigquery.SchemaField("InAppAdInstanceId", "INTEGER"),
            bigquery.SchemaField("AbTestGroup", "STRING"),
            bigquery.SchemaField("Revenue", "FLOAT"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
        ],
        overwrite=False,
    )
    
    dedup_query = """
    create or replace table DWH.FactAdRevenueImpressionLevel  as
    select * except(rn) from(
    select *, row_number() over (partition by Time, CountryId, IDFA, IDFV, InAppAdSegmentId, IronsrcAppId, InAppAdTypeId, InAppAdPlacementId, InAppAdNetworkId,InAppAdInstanceId,appinternalid ) rn
    from DWH.FactAdRevenueImpressionLevel) where rn=1 
    """
    
    dml_for_bq(dedup_query, output=True)