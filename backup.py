def clear_bq_backups():
    
    from google.cloud import storage
    import gcsfs
    import time
    import pandas as pd
    from datetime import datetime, timedelta

    project = "nebula-horoscope"
    key = "key.json"
    gcs = gcsfs.GCSFileSystem(project=project, token=key, cache_timeout=0)
    
    storage_client = storage.Client.from_service_account_json(key)
    bucket_name = "bq-etl-nebula"
    bucket = storage_client.bucket(bucket_name)
    prefix = "BQ-BackUps/"

    all_blobs = list(storage_client.list_blobs(bucket, prefix=prefix))

    gcs.clear_instance_cache()
    
    blob_list=[]
    for i in all_blobs:
        if (pd.to_datetime(i.updated).strftime("%Y-%m-%d") >= (datetime.now().date() - timedelta(days=45)).isoformat()
        and (str(i.name).endswith('.csv') or (str(i.name).endswith('.avro')))):
            i.delete()
            print('Blob' + i.name + ' removed.')
            blob_list.append(str(i.name))
            
    clear_bq_backups.blobs_size = len(blob_list)
    
    print('{} expired BackUps cleared!'.format(clear_bq_backups.blobs_size))
    
    
def backup_bq(dataset="DWH", destination_format="json"):
    from google.cloud import bigquery, storage
    from time import strftime, gmtime
    key = "key.json"
    bq_client = bigquery.Client.from_service_account_json(key)
    project = "nebula-horoscope"

    prefix = "gs://"
    dataset_id = dataset
    gs_bucket = "bq-etl-nebula"
    gs_folder = "BQ-BackUps"
    tables = bq_client.list_tables(dataset_id)
    destination_extension = ".json" if destination_format == "json" else ".csv"
    tables_list=[]
    for table in tables:
        if 'vT' not in table.table_id:
            print(table.table_id + " is being backed up...")
            table_name = table.table_id
            destination_table_name = table_name + str(strftime("%Y%m%d_%H%M%S", gmtime()))
            destination_file = "/" + destination_table_name + "_*" + destination_extension
            gs_subfolder = table_name
            gs_path = prefix + gs_bucket + "/" + gs_folder + "/" + gs_subfolder

            destination_uri = gs_path + "{0}".format(destination_file)
            dataset_ref = bq_client.dataset(dataset_id, project=project)
            table_ref = dataset_ref.table(table_name)

            job_config = bigquery.job.ExtractJobConfig()
            if destination_format == "NEWLINE_DELIMITED_JSON":
                job_config.compression = bigquery.Compression.JSON
                job_config.destination_format = bigquery.DestinationFormat.NEWLINE_DELIMITED_JSON
            elif destination_format == "csv":
                job_config.compression = bigquery.Compression.GZIP
                job_config.destination_format = bigquery.DestinationFormat.CSV

            extract_job = bq_client.extract_table(
                table_ref, destination_uri, job_config=job_config, location="US"
            )
            extract_job.result()
            print(table.table_id + " saved to " + gs_path)
            tables_list.append(str(table.table_id))
        
    backup_bq.tables_num = len(tables_list)
    print('{} tables backed up to GCS!'.format(backup_bq.tables_num))
     