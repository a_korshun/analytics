def appodeal_revenue():
    
    from google.cloud import bigquery, storage
    import requests
    import json
    from tqdm import tqdm
    import pandas as pd
    import time
    import logging
    from datetime import datetime, timedelta
    import numpy as np

    from helpers.data_processing import get_billing_events
    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql, dml_for_bq
    from clients.appodeal import Appodeal

    api_token = "b51dd49e89bae7e3fa646ec5e1c7082d"
    user_id = 102910

    ap = Appodeal(api_token, user_id)

    date_from = (datetime.now().date() - timedelta(days=1)).isoformat()
    date_to = (datetime.now().date() - timedelta(days=0)).isoformat()

    appodeal_data = ap.report(date_from=date_from, date_to=date_to)

    adv_report = appodeal_data.copy()

    adv_report.drop(columns=["package_name", "app_key"], inplace=True)

    adv_report["app_name"] = adv_report["app_name"].apply(
        lambda x: "Nebula: Horoscope & Astrology" if "Nebula" in x else 'TikGlory'
    )

    adv_report["platform"] = adv_report["platform"].apply(
        lambda x: "ios" if x == "Apple" else 'android'
    )

    adv_report["country_code"] = adv_report["country_code"].replace(["ZZ", "NA"], "Unknown")

    object_columns = [
        "app_name",
        "network",
        "ad_type",
        "platform",
        "segment",
        "placement",
        "country_code",
    ]

    for col in object_columns:
        adv_report[col] = adv_report[col].replace([0, -1, "None", np.nan], "Unknown")

    numeric_columns = [
        "requests",
        "fills",
        "impressions",
        "fillrate",
        "clicks",
        "ctr",
        "views",
        "revenue",
        "ecpm",
    ]

    decimal_columns = [
        "fillrate",
        "ctr",
        "revenue",
        "ecpm",
    ]

    for col in numeric_columns:
        adv_report[col] = adv_report[col].replace([0, -1, "None", np.nan, "Unknown"], 0)

    for col in decimal_columns:
        adv_report[col] = adv_report[col].round(2)
        

    adv_report.rename(
        columns={
            "app_name": "AppId",
            "country_code": "CountryId",
            "date": "Date",
            "network": "InAppAdNetworkId",
            "ad_type": "InAppAdTypeId",
            "platform": "PlatformId",
            "segment": "InAppAdSegmentId",
            "placement": "InAppAdPlacementId",
            "requests": "Requests",
            "fills": "Fills",
            "fillrate": "FillRate",
            "impressions": "Impressions",
            "clicks": "Clicks",
            "ctr": "CTR",
            "views": "Views",
            "revenue": "Revenue",
            "ecpm": "eCPM",
        },
        inplace=True,
    )
    
    update_dim(
        data_df=adv_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    update_dim(
        data_df=adv_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )
    
    update_dim(
        data_df=adv_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )

    update_dim(
        data_df=adv_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )

    map_dim(
        dim="DimApp",
        data_df=adv_report,
        cat_column_df="AppId",
        cat_column_dim="AppName",
        id_column_dim="AppInternalId",
    )

    map_dim(
        dim="DimCountry",
        data_df=adv_report,
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )
    
    map_dim(
        dim="DimPlatform",
        data_df=adv_report,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )
    
    map_dim(
        data_df=adv_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    map_dim(
        data_df=adv_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )
    
    map_dim(
        data_df=adv_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )
    
    map_dim(
        data_df=adv_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )
    
    adv_report["Date"] = pd.to_datetime(adv_report["Date"])

    appodeal_revenue.report_size = len(adv_report)
    
    appodeal_revenue.target_table = 'FactAdRevenue'

    df_to_bq(
        adv_report,
        table=appodeal_revenue.target_table,
        schema=[
            bigquery.SchemaField("Date", "Date"),
            bigquery.SchemaField("AppId", "INTEGER"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("InAppAdNetworkId", "INTEGER"),
            bigquery.SchemaField("InAppAdTypeId", "INTEGER"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("InAppAdSegmentId", "INTEGER"),
            bigquery.SchemaField("InAppAdPlacementId", "INTEGER"),
            bigquery.SchemaField("Requests", "INTEGER"),
            bigquery.SchemaField("Fills", "INTEGER"),
            bigquery.SchemaField("Impressions", "INTEGER"),
            bigquery.SchemaField("FillRate", "FLOAT"),
            bigquery.SchemaField("Clicks", "INTEGER"),
            bigquery.SchemaField("CTR", "FLOAT"),
            bigquery.SchemaField("Views", "INTEGER"),
            bigquery.SchemaField("Revenue", "FLOAT"),
            bigquery.SchemaField("eCPM", "FLOAT"),
        ],
        overwrite=False,
    ) 