def mysql_payments():

    import os
    import time
    import warnings
    from datetime import datetime, timedelta
    from time import gmtime, strftime
    import json

    import dask.dataframe as dd
    from dask.diagnostics import ProgressBar
    import gcsfs
    import numpy as np
    import pandas as pd
    import re
    import requests

    from google.cloud import bigquery, storage
    from pandas.api.types import CategoricalDtype
    from pandas.testing import assert_frame_equal
    from tqdm import tqdm
    import pymysql
    from sqlalchemy import create_engine

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql, dml_for_bq 

    warnings.filterwarnings("ignore")

    iaps = df_from_mysql(
   """
    with rt as (            
                            select ip.purchase_date Time
                                ,case when product_id ='weekly_9.99_0.49_7d_subscription' 
                                            and row_number() over (partition by u.id, sl.bundle_name, Case when ip.trial = 1 then 'trial' else 'regular' end order by ip.purchase_date) - 1  = 0 then 0.49
                                      when product_id ='monthly_14.99_0t_subscription' 
                                            and row_number() over (partition by u.id, sl.bundle_name, Case when ip.trial = 1 then 'trial' else 'regular' end order by ip.purchase_date) - 1  = 0 then 9.99
                                      else sl.price_usd 
                                 end UsdGross
                                ,'USD' as CurrencyId
                                ,u.device_appsflyer_id UserAppsflyerId
                                ,u.device_idfa IDFA
                                ,u.device_idfv IDFV
                                ,u.id UserClientId
                                ,u.platform PlatformId
                                ,ip.receipt_id
                                ,0 as AppInternalId
                                ,sl.`length` as SubscriptionDurationId
                                ,ip.transaction_id TransactionId 
                                ,sl.trial_days as TrialDuration
                                ,sl.bundle_name as ProductId
                                ,Case when ip.trial = 1 then 'trial'
                                        else 'regular' 
                                 end as TransactionTypeId
                                ,row_number() over (partition by u.id, sl.bundle_name, Case when ip.trial = 1 then 'trial' else 'regular' end order by ip.purchase_date) - 1 as RebillPeriod
                            from itunes_purchase ip 
                            join itunes_receipt ir on ip.receipt_id = ir.id 
                            left join device d on d.id = ir.device_id 
                            left join users u on u.id = d.user_id 
                            left join subscriptions_list sl on sl.bundle_name = ip.product_id  
                            )
    select * 
    from (
            select ip.cancellation_date Time
                ,0 - rt.UsdGross UsdGross
                ,'USD' as CurrencyId
                ,u.device_appsflyer_id UserAppsflyerId
                ,u.device_idfa IDFA
                ,u.device_idfv IDFV
                ,u.id UserClientId
                ,u.platform PlatformId
                ,ip.receipt_id
                ,0 as AppInternalId
                ,sl.`length` as SubscriptionDurationId
                ,ip.transaction_id TransactionId 
                ,sl.trial_days as TrialDuration
                ,sl.bundle_name as ProductId
                ,'refund' as TransactionTypeId
                ,rt.RebillPeriod
            from itunes_purchase ip 
            join itunes_receipt ir on ip.receipt_id = ir.id 
            left join device d on d.id = ir.device_id 
            left join users u on u.id = d.user_id 
            left join subscriptions_list sl on sl.bundle_name = ip.product_id 
            left join rt on rt.TransactionId = ip.transaction_id and ip.receipt_id = rt.receipt_id
            where ip.cancellation_date is not null 
            union all 
            select *
            from rt
            ) p
   where date(p.time) >= date_add(current_date(), interval -3 day)
           
        """
    , port=3308)

    pays = iaps.copy()
    pays.reset_index(drop=True, inplace=True)
    pays = pays[~pays.UsdGross.isna()]
    pays["ProductTypeId"] = "Subscription"

    object_columns = [
        "CurrencyId",
        "PlatformId",
        "ProductTypeId",
        "ProductId",
        "ProductTypeId",
        "TransactionTypeId",
        "SubscriptionDurationId",
    ]

    for col in pays[object_columns]:
        pays[col] = pays[col].replace([0, -1, "None", np.nan, " ", ""], "Unknown")

    other_columns = np.setdiff1d(
        pays.columns.tolist(), object_columns + ["UsdGross"] + ['AppInternalId']
    ).tolist()

    other_columns.remove("RebillPeriod")

    for col in other_columns:
        pays[col] = pays[col].replace([0, -1, "None", "Unknown", np.nan, " ", ""], np.nan)

    update_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )

    map_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )

    update_dim(
        data_df=pays,
        dim="DimProductType",
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )

    map_dim(
        dim="DimProductType",
        data_df=pays,
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )

    update_dim(
        data_df=pays,
        dim="DimProduct",
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )

    map_dim(
        dim="DimProduct",
        data_df=pays,
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )

    update_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )

    map_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )

    map_dim(
        dim="DimCountry",
        data_df=pays,
        cat_column_df="CurrencyId",
        cat_column_dim="CurrencyCode",
        id_column_dim="CurrencyId",
    )
    pays["CurrencyId"] = pays["CurrencyId"].astype(int)

    map_dim(
        dim="DimPlatform",
        data_df=pays,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )

    map_dim(
        dim="DimApp",
        data_df=pays,
        cat_column_df="AppInternalId",
        cat_column_dim="AppName",
        id_column_dim="AppInternalId",
    )

    pays["UsdGross"] = pays["UsdGross"].astype(float)
    pays["TransactionId"] =  'AP'+pays["TransactionId"].astype(str)
    pays["Time"] = pd.to_datetime(pays.Time)

    pays = pays[
        [
            "Time",
            "TransactionId",
            "UsdGross",
            "CurrencyId",
            "UserAppsflyerId",
            "IDFA",
            "IDFV",
            "UserClientId",
            "PlatformId",
            "AppInternalId",
            "SubscriptionDurationId",
            "TrialDuration",
            "RebillPeriod",
            "ProductTypeId",
            "ProductId",
            "TransactionTypeId",
        ]
    ]

    pays['RebillPeriod'][pays.TransactionTypeId==1] = 0 #trial

    pays['UsdGross'][(pays.ProductId!=31) & (pays.ProductId!=15) & (pays.TransactionTypeId==1)] = 0

    #pays['TransactionTypeId'][(pays.ProductId!=31) & (pays.ProductId!=15)  & (pays.RebillPeriod==0) & (pays.UsdGross>0)] = 1
    # pays['TransactionTypeId'][(pays.ProductId!=31) & (pays.ProductId!=15) & (pays.RebillPeriod==0) & (pays.UsdGross<0)] = 2

    pays['RebillPeriod'][(pays.TransactionTypeId.isin([0,2]))] = pays['RebillPeriod'][(pays.TransactionTypeId.isin([0,2]))]+1
    #Editted subscriptions fix

    pays['UsdGross'][(pays.ProductId==27) & (pays.RebillPeriod>0) & (pays.TransactionTypeId==0)] = 7.99
    pays['UsdGross'][(pays.ProductId==27) & (pays.RebillPeriod>0) & (pays.TransactionTypeId==2)] = -7.99
    
    pays['AppInternalId'] = 0

    mysql_payments.report_size = len(pays)
    
    mysql_payments.target_table = 'FactTransaction'

    df_to_bq(
        pays,
        table=mysql_payments.target_table,
        schema=[
            bigquery.SchemaField("Time", "TIMESTAMP"),
            bigquery.SchemaField("TransactionId", "STRING"),
            bigquery.SchemaField("UsdGross", "FLOAT"),
            bigquery.SchemaField("CurrencyId", "INTEGER"),
            bigquery.SchemaField("UserAppsflyerId", "STRING"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("UserClientId", "INTEGER"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("SubscriptionDurationId", "INTEGER"),
            bigquery.SchemaField("TrialDuration", "FLOAT"),
            bigquery.SchemaField("RebillPeriod", "INTEGER"),
            bigquery.SchemaField("ProductTypeId", "INTEGER"),
            bigquery.SchemaField("ProductId", "INTEGER"),
            bigquery.SchemaField("TransactionTypeId", "INTEGER"),
        ],
        overwrite=False,
    )

 