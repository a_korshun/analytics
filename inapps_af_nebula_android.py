def appsflyer_inapps_android():
    
    import os
    import time
    import warnings
    from datetime import datetime, timedelta
    from time import gmtime, strftime
    import json

    import dask.dataframe as dd
    from dask.diagnostics import ProgressBar
    import gcsfs
    import numpy as np
    import pandas as pd
    import re
    import requests

    from google.cloud import bigquery, storage
    from pandas.api.types import CategoricalDtype
    from pandas.testing import assert_frame_equal
    from tqdm import tqdm
    import pymysql
    from sqlalchemy import create_engine

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql, dml_for_bq
    from clients.appsflyer import AppsFlyer

    warnings.filterwarnings("ignore")

    primary_token = "29d85f91-d29b-4b43-a62d-e65b0633efef"
    app_id = "genesis.nebula"
    af = AppsFlyer(api_token=primary_token, app_id=app_id)

    events = "subscription_lt_start_success,iap_purchase_success"

    iaps = pd.DataFrame()
    for d in tqdm(
        pd.date_range(
            (datetime.now().date() - timedelta(days=5)).isoformat(),
            (datetime.now().date()).isoformat(),
            freq="4D",
        ).tolist()
    ):
        resp_organic = af.organic_in_app_events_report(
            date_from=d.strftime("%Y-%m-%d"),
            date_to=(d + timedelta(days=4)).strftime("%Y-%m-%d"),
            as_df=True,
            event_name=events,
        )

        resp = af.in_app_events_report(
            date_from=d.strftime("%Y-%m-%d"),
            date_to=(d + timedelta(days=4)).strftime("%Y-%m-%d"),
            as_df=True,
            event_name=events,
        )

        iaps = pd.concat([iaps, resp_organic, resp])
        time.sleep(1)
        print(d.strftime("%Y-%m-%d"))

    pays = iaps[iaps['Event Time']>='2020-07-29'].copy()
    pays.reset_index(drop=True, inplace=True)

    pays = pays[
        [
            "Event Time",
            "Event Name",
            "Event Value",
            "Event Revenue",
            "Event Revenue Currency",
            "Event Revenue Preferred",
            "Event Source",
            "AppsFlyer ID",
            "Advertising ID",
            "IDFV",
            "Customer User ID",
            "Platform", 
        ]
    ]

    pp = pd.json_normalize(pays["Event Value"].map(json.loads))

    param_cols = [
        "subscription_id",
        "product_id",
        "af_content_id",
        "SubscriptionDurationId",
        "TrialDuration",
        "RebillPeriod",
        "TransactionId",
    ]
    for c in param_cols:
        if c not in pp.columns:
            pp[c] = np.nan

    pp["ProductId"] = pp.af_content_id.combine_first(pp.subscription_id).combine_first(
        pp.product_id
    )

    pays = pays.join(pp)

    inapp = pays[(pays["Event Name"] == "iap_purchase_success") ]
    pays = pays[pays["Event Name"] != "iap_purchase_success"]

    iapp_list = inapp[inapp["Event Name"] == "iap_purchase_success"]["ProductId"].unique()
    iapp_price_map = dict.fromkeys(iapp_list)
    p = "[\d]+[.,\d]+|[\d]*[.][\d]+|[\d]+"
    for val in iapp_list:
        if re.search(p, val) is not None:
            for catch in re.finditer(p, val):
                iapp_price_map[val] = catch[0]

    iapp_price_map['lifetime_119.99_0t'] = 119.99

    pays = pd.concat([pays, inapp])

    pays["Event Revenue"] = pays["ProductId"].map(iapp_price_map)

    pays["Event Revenue"].fillna(0, inplace=True)

    pays["Event Time"] = pd.to_datetime(pays["Event Time"])

    pays.drop(
        columns=[
            "af_content_id",
            "subscription_id",
            "product_id", 
            "Event Value",
            "Event Revenue Preferred",
            "Event Source", 
        ],
        inplace=True,
    )

    pays["af_content_type"] = pays["Event Name"].apply(
        lambda x: "InApp" if x == "iap_purchase_success" else "Subscription"
    )

    pays.rename(
        columns={
            "Event Time": "Time",
            "Event Name": "EventId",
            "Event Revenue": "UsdGross",
            "Event Revenue Currency": "CurrencyId",
            "AppsFlyer ID": "UserAppsflyerId",
            "Customer User Id": "UserClientId",
            "Customer User ID": "UserClientId",
            "Platform": "PlatformId", 
            "af_content_type": "ProductTypeId",
            "subscription_type": "SubscriptionDurationId",
            "rebill_period": "RebillPeriod",
            "trial_duration": "TrialDuration",
            "transaction_id": "TransactionId",
            "Advertising ID":'IDFA'
        },
        inplace=True,
    )

    event_type = {
        "billing_subscription_success": "regular",
        "iap_purchase_success": "regular",
        "billing_subscription_trial_success": "trial",
        "back_billing_refund_subscription": "refund",
        "subscription_lt_start_success":'regular'
    }

    object_columns = [
        "CurrencyId",
        "PlatformId",
        "ProductTypeId",
        "ProductId",
        "ProductTypeId", 
        "SubscriptionDurationId",
    ]

    for col in pays[object_columns]:
        pays[col] = pays[col].replace([0, -1, "None", np.nan], "Unknown")

    other_columns = np.setdiff1d(
        pays.columns.tolist(), object_columns + ["UsdGross"]
    ).tolist()

    for col in other_columns:
        pays[col] = pays[col].replace([0, -1, "None", "Unknown", np.nan], np.nan)
        
    pays['AppInternalId'] = 0

    pays["UsdGross"] = pays["UsdGross"].astype(float)
    pays["TransactionId"] = pays["TransactionId"].astype(float)
    pays["Time"] = pd.to_datetime(pays.Time)

    #THE FOLLOWING IS RELEVANT FOR LT ONLY
    pays = pays[pays.ProductId!='weekly_9.99_3t_subscription']
    pays["TransactionTypeId"] = pays["EventId"].map(event_type)
    pays['SubscriptionDurationId'][pays.EventId=='subscription_lt_start_success'] = 'lifetime' 
    pays['RebillPeriod'] = np.nan
    pays['TrialDuration'] = np.nan

    update_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )

    map_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )

    update_dim(
        data_df=pays,
        dim="DimProductType",
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )

    map_dim(
        dim="DimProductType",
        data_df=pays,
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )

    update_dim(
        data_df=pays,
        dim="DimProduct",
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )

    map_dim(
        dim="DimProduct",
        data_df=pays,
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )

    update_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )

    map_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )

    map_dim(
        dim="DimCountry",
        data_df=pays,
        cat_column_df="CurrencyId",
        cat_column_dim="CurrencyCode",
        id_column_dim="CurrencyId",
    )
    pays["CurrencyId"] = pays["CurrencyId"].astype(int)

    map_dim(
        dim="DimPlatform",
        data_df=pays,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    ) 

    pays = pays[
        [
            "Time",
            "TransactionId",
            "UsdGross",
            "CurrencyId",
            "UserAppsflyerId",
            "IDFA",
            "IDFV",
            "UserClientId",
            "PlatformId",
            "AppInternalId",
            "SubscriptionDurationId",
            "TrialDuration",
            "RebillPeriod",
            "ProductTypeId",
            "ProductId",
            "TransactionTypeId",
        ]
    ]
    
    appsflyer_inapps_android.report_size = len(pays)
    
    appsflyer_inapps_android.target_table = 'FactTransaction'

    df_to_bq(
        pays,
        table=appsflyer_inapps_android.target_table,
        schema=[
            bigquery.SchemaField("Time", "TIMESTAMP"),
            bigquery.SchemaField("TransactionId", "STRING"),
            bigquery.SchemaField("UsdGross", "FLOAT"),
            bigquery.SchemaField("CurrencyId", "INTEGER"),
            bigquery.SchemaField("UserAppsflyerId", "STRING"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("UserClientId", "INTEGER"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("SubscriptionDurationId", "INTEGER"),
            bigquery.SchemaField("TrialDuration", "FLOAT"),
            bigquery.SchemaField("RebillPeriod", "INTEGER"),
            bigquery.SchemaField("ProductTypeId", "INTEGER"),
            bigquery.SchemaField("ProductId", "INTEGER"),
            bigquery.SchemaField("TransactionTypeId", "INTEGER"),
        ],
        overwrite=False,
    )

