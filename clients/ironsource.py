import requests
from furl import furl
import pandas as pd
from datetime import datetime
import io
import os


class Ironsrc:

    AUTH_ENDPOINT = "https://platform.ironsrc.com/partners/publisher/auth?"
    REPORT_ENDPOINT = "https://platform.ironsrc.com/partners/publisher/mediation/applications/v6/stats?"
    REVENUE_IMPRESSION_LEVEL_ENDPOINT = (
        "https://platform.ironsrc.com/partners/adRevenueMeasurements/v1?"
    )
    REVENUE_USER_LEVEL_ENDPOINT = (
        "https://platform.ironsrc.com/partners/userAdRevenue/v3?"
    )

    METRICS = ",".join(
        [
            "clicks",
            "impressions",
            "completions",
            "clickThroughRate",
            "appRequests",
            "appFillRate",
            "revenue",
            "eCPM",
        ]
    )

    BREAKDOWNS = ",".join(
        [
            "date",
            "app",
            "platform",
            "adSource",
            "adUnits",
            # "instance",
            "country",
            # "segment",
            # "placement",
        ]
    )

    def __init__(self, secretkey, refreshToken):
        self.secretkey = secretkey
        self.refreshToken = refreshToken
        self.headers = {"secretkey": self.secretkey, "refreshToken": self.refreshToken}
        self.bearer_token = requests.get(
            self.AUTH_ENDPOINT, headers=self.headers
        ).json()
        self.bearer_headers = {"Authorization": "Bearer " + self.bearer_token} 

    def __build_args(self, date_from, date_to, kwargs):

        args = {
            "startDate": date_from,
            "endDate": date_to,
        }

        if "appKey" in kwargs:
            args["appKey"] = kwargs.get("appKey")

        if "metrics" in kwargs:
            args["metrics"] = kwargs.get("metrics")

        if "breakdowns" in kwargs:
            args["breakdowns"] = kwargs.get("breakdowns")

        if "adUnits" in kwargs:
            args["adUnits"] = kwargs.get("adUnits")

        if "adSource" in kwargs:
            args["adSource"] = kwargs.get("adSource")

        if "country" in kwargs:
            args["country"] = kwargs.get("country")

        return args

    def __to_df(self, resp):
        import pandas as pd

        if resp.status_code != requests.codes.ok:
            print(resp.status_code)
            raise Exception(resp.text)

        df = pd.json_normalize(resp.json())
        main_df = pd.DataFrame()
        json_df = pd.DataFrame()

        for i, r in df.iterrows():

            for d in df.loc[i, "data"]:

                row = pd.DataFrame(df.loc[i]).T
                main_df = pd.concat([main_df, row])

                json = pd.json_normalize(d)
                json_df = pd.concat([json_df, json])

        main_df.reset_index(inplace=True, drop=True)
        json_df.reset_index(inplace=True, drop=True)

        data = pd.concat([main_df, json_df], axis=1)

        data.drop(columns="data", inplace=True)

        return data

    def __download_file(self, url):

        output = io.BytesIO()
        output.name = "chunk_{}.csv.gzip".format(datetime.now().isoformat())
        with requests.get(url, stream=True) as resp:
            resp.raise_for_status()
            for chunk in resp.iter_content(chunk_size=None):
                if chunk:
                    output.write(chunk)
        output.seek(0)

        return output

    def report(
        self, date_from, date_to, as_df=True, metrics=None, breakdowns=None, **kwargs
    ):

        f = furl(self.REPORT_ENDPOINT)

        if metrics is None:
            kwargs["metrics"] = self.METRICS
        if breakdowns is None:
            kwargs["breakdowns"] = self.BREAKDOWNS

        f.args = self.__build_args(date_from, date_to, kwargs)

        resp = requests.get(f.url, headers=self.bearer_headers)

        if as_df:
            return self.__to_df(resp)

    def detailed_report(self, date, appKey, report_type, **kwargs):

        valid_report_types = ["user_level", "impression_level"]

        if report_type == "user_level":
            report_endpoint = self.REVENUE_USER_LEVEL_ENDPOINT
        elif report_type == "impression_level":
            report_endpoint = self.REVENUE_IMPRESSION_LEVEL_ENDPOINT
        else:
            raise ValueError(
                "ReportType is invalid. It must be one of {}".format(valid_report_types)
            )

        f = furl(report_endpoint)

        args = {"date": date, "appKey": appKey}
        if report_type == "user_level":
            args["reportType"] = 1

        f.args = args

        resp = requests.get(f.url, headers=self.bearer_headers)

        if resp.status_code != requests.codes.ok:
            print(resp.status_code)
            raise Exception(resp.text)
        else:
            resp = resp.json()

        report = pd.DataFrame()
        for i in resp["urls"]:
            file_csv = pd.read_csv(self.__download_file(i), compression="gzip")
            report = pd.concat([report, file_csv])

        return report