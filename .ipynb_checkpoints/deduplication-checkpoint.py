def dedup():
    from helpers.data_etl import dml_for_bq, df_from_bq
    from tqdm import tqdm
    import pandas as pd

    FactTransaction = """
    create or replace table DWH.FactTransaction  as
    select * except(rn) from(
    select *, row_number() over (partition by Time, UserAppsflyerId, UserClientId, ProductId, TransactionTypeId, Producttypeid, TransactionId, appinternalid) rn
from DWH.FactTransaction) where rn=1
    """

    DimUser = """
    create or replace table DWH.DimUser  as
    select * except(rn) from(
    select *, row_number() over (partition by UserClientId, UserAppsflyerId, IDFA, InstallTime,AttributionEventTime, countryid) rn
    from DWH.DimUser) where rn=1 
    """

    FactAdRevenue = """
    create or replace table DWH.FactAdRevenue  as
    select * except(rn) from(
    select *, row_number() over (partition by AppId, CountryId, Date, InAppAdNetworkId, InAppAdTypeId, PlatformId, InAppAdSegmentId, InAppAdPlacementId ) rn
    from DWH.FactAdRevenue) where rn=1 
    """

    #     FactMarketingSpent = """
    #     create or replace table DWH.FactMarketingSpent  as
    #     select * except(rn) from(
    #     select *, row_number() over (partition by Date, CountryId, AgencyId, MediaSourceId, CampaignInternalId, PublisherInternalId, AdSetInternalId, AdInternalid, AdInternalId, clicks, impressions, installs, cast(AdvCost as int64) order by installs desc ) rn
    #     from DWH.FactMarketingSpent) where rn=1
    #     """

    data_dict = {}
    data_dict["table"] = [
        "FactAdRevenue",
    #    "FactMarketingSpent",
        "FactTransaction",
        "DimUser",
    ]
    data_dict["query"] = [
        FactAdRevenue, 
    #    FactMarketingSpent, 
        FactTransaction, 
        DimUser
    ]
    data_df = pd.DataFrame(data_dict)
    data_df.set_index("table", inplace=True)

    for q in tqdm(data_df.index):
        data_df.loc[q, "before"] = int(
            df_from_bq("""select count(1) from DWH.{}""".format(q), output=False).values
        )
        dml_for_bq(data_df.loc[q, "query"], output=True)
        data_df.loc[q, "after"] = int(
            df_from_bq("""select count(1) from DWH.{}""".format(q), output=False).values
        )
        data_df.loc[q, "removed"] = data_df.loc[q, "before"] - data_df.loc[q, "after"]

    dedup.du_removed = float(data_df.loc["DimUser", "removed"])
    dedup.fm_removed = 0#float(data_df.loc["FactMarketingSpent", "removed"])
    dedup.fad_removed = float(data_df.loc["FactAdRevenue", "removed"])
    dedup.ft_removed = float(data_df.loc["FactTransaction", "removed"])