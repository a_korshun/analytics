from prod_appodeal_advrevenue import appodeal_revenue
from prod_ironsrc_advrevenue import ironsrc_revenue

from prod_appsflyer_costs_import import appsflyer_cost
from prod_appsflyer_costs_import_android import appsflyer_cost_android
from prod_additional_costs_import import additional_costs

from prod_appsflyer_installs import appsflyer_installs
from prod_appsflyer_organic_installs import appsflyer_organic_installs
from prod_appsflyer_installs_android import appsflyer_installs_android
from prod_appsflyer_organic_installs_android import appsflyer_organic_installs_android

from prod_inapps_appsflyer import appsflyer_inapps
from prod_inapps_appsflyer_android import appsflyer_inapps_android
from prod_payments_mysql import mysql_payments
from prod_payments_mysql_android import mysql_payments_android

from deduplication import dedup
from backup import backup_bq, clear_bq_backups
from prod_firebase_to_gcs import firebase_to_gcs

from helpers.messaging import msg_to_slack_me, format_slack_attachment

from prod_ironsrc_userlevel_revenue import ironsrc_revenue_userlevel
from prod_ironsrc_imprlevel_revenue import ironsrc_revenue_impressionlevel

import pandas as pd
from datetime import datetime

import logging
from google.cloud import logging as cloudlogging

log_client = cloudlogging.Client.from_service_account_json("key.json")
log_handler = log_client.get_default_handler()
cloud_logger = logging.getLogger("cloudLogger")
cloud_logger.setLevel(logging.INFO)
cloud_logger.addHandler(log_handler)


def appodeal_revenue_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppodealRevenue"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appodeal_revenue()

    attachment = format_slack_attachment(
        gcf_function,
        appodeal_revenue.target_table,
        "*Inserted:*\n {}".format(str(appodeal_revenue.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def ironsrc_revenue_import(event, context):
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-IronscrRevenue"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    ironsrc_revenue()

    attachment = format_slack_attachment(
        gcf_function,
        ironsrc_revenue.target_table,
        "*Inserted:*\n {}".format(str(ironsrc_revenue.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def ironsrc_revenue_userlevel_import(event, context):
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-IronsrcRevenueUserLevel"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    ironsrc_revenue_userlevel()

    attachment = format_slack_attachment(
        gcf_function,
        ironsrc_revenue_userlevel.target_table,
        "*Inserted:*\n {}".format(str(ironsrc_revenue_userlevel.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def ironsrc_revenue_impressionlevel_import(event, context):
    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-IronsrcRevenueImpressionLevel"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    ironsrc_revenue_impressionlevel()

    attachment = format_slack_attachment(
        gcf_function,
        ironsrc_revenue_impressionlevel.target_table,
        "*Inserted:*\n {}".format(
            str(ironsrc_revenue_impressionlevel.report_size) + " rows"
        ),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_cost_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerCosts"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_cost()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_cost.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_cost.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_cost_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerCostAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_cost_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_cost_android.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_cost_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)
    
def doc_additional_costs_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AdditionalCostsImport"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    additional_costs()

    attachment = format_slack_attachment(
        gcf_function,
        additional_costs.target_table,
        "*Inserted:*\n {}".format(str(additional_costs.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_installs_organic_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsOrganic"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_organic_installs()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_organic_installs.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_organic_installs.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_installs_organic_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsOrganicAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_organic_installs_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_organic_installs_android.target_table,
        "*Inserted:*\n {}".format(
            str(appsflyer_organic_installs_android.report_size) + " rows"
        ),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_installs_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstalls"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_installs()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_installs.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_installs.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_installs_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInstallsAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_installs_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_installs_android.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_installs_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def mysql_payments_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-MySqlPayments"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    mysql_payments()

    attachment = format_slack_attachment(
        gcf_function,
        mysql_payments.target_table,
        "*Inserted:*\n {}".format(str(mysql_payments.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def mysql_payments_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-MysqlPaymentsAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    mysql_payments_android()

    attachment = format_slack_attachment(
        gcf_function,
        mysql_payments_android.target_table,
        "*Inserted:*\n {}".format(str(mysql_payments_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_inapps_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInapps"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_inapps()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_inapps.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_inapps.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def appsflyer_inapps_android_import(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-AppsflyerInappsAndroid"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    appsflyer_inapps_android()

    attachment = format_slack_attachment(
        gcf_function,
        appsflyer_inapps_android.target_table,
        "*Inserted:*\n {}".format(str(appsflyer_inapps_android.report_size) + " rows"),
    )
    msg_to_slack_me(message="", attachment=attachment)


def deduplicate(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-Deduplicate"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    dedup()

    message = """*Removed:* \n DimUser -> {0} \n FactTransaction -> {1} \n FactAdRevenue -> {2} \n FactMarketingSpent -> {3}""".format(
        dedup.du_removed, dedup.ft_removed, dedup.fad_removed, dedup.fm_removed
    )

    attachment = format_slack_attachment(gcf_function, "Fact Tables", message)
    msg_to_slack_me(message="", attachment=attachment)


def backup_bigquery(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-BackUpBQ"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    backup_bq()

    message = """*Backed Up:*\n {0} tables to GCS""".format(str(backup_bq.tables_num))

    attachment = format_slack_attachment(gcf_function, "DWH", message)
    msg_to_slack_me(message="", attachment=attachment)


def remove_expired_backups(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-ClearBackUps"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    clear_bq_backups()

    message = """*Removed:*\n{} files from GCS""".format(str(clear_bq_backups.blobs_size))

    attachment = format_slack_attachment(gcf_function, "Cloud Storage", message)

    msg_to_slack_me(message="", attachment=attachment)
    
def firebase_bq_export(event, context):

    gcf_starttime = pd.to_datetime(context.timestamp).tz_localize(None)
    gcf_function = "ETL-FirebaseBqExport"
    gcf_trigger_mess = context.event_id

    print(
        "GCF {0} trigger by message {1} started at {2}!".format(
            gcf_function, gcf_trigger_mess, gcf_starttime
        )
    )

    firebase_to_gcs()

    message = """*Exported:*\n{} tables(s) to GCS""".format(str(firebase_to_gcs.tables_num))

    attachment = format_slack_attachment(gcf_function, "Cloud Storage", message)

    msg_to_slack_me(message="", attachment=attachment)