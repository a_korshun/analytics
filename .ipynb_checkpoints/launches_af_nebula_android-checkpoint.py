def launches_extract_android():
        
    import os
    import time
    import warnings
    from datetime import datetime, timedelta
    from time import gmtime, strftime

    import numpy as np
    import pandas as pd

    from google.cloud import bigquery, storage
    from pandas.testing import assert_frame_equal
    from tqdm import tqdm

    from helpers.dimensions import update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, dml_for_bq
    from clients.appsflyer import AppsFlyer

    warnings.filterwarnings("ignore")

    primary_token = "29d85f91-d29b-4b43-a62d-e65b0633efef"
    app_id = "genesis.nebula"
    af = AppsFlyer(api_token=primary_token, app_id=app_id)

    events = "launch,launch_first_time"
    days_back = 1

    launch = pd.DataFrame()

    d = (datetime.now().date() - timedelta(days=days_back))

    resp_organic = af.organic_in_app_events_report(
        date_from=d.strftime("%Y-%m-%d"),
        date_to=d.strftime("%Y-%m-%d"),
        as_df=True,
        event_name=events,
    )

    resp = af.in_app_events_report(
        date_from=d.strftime("%Y-%m-%d"),
        date_to=d.strftime("%Y-%m-%d"),
        as_df=True,
        event_name=events,
    )

    launch = pd.concat([launch, resp_organic, resp])
    time.sleep(1)
    print(d.strftime("%Y-%m-%d"))
    
    launch_data = launch.copy()
    launch_data = launch_data[
        [
            "Event Time",
            "Event Name",
            "Install Time",
            "AppsFlyer ID",
            "WIFI",
            "IDFA",
            "Android ID",
            "IDFV",
            "Customer User ID",
            "Platform",
            "Device Type",
            "OS Version",
            "App ID",
            "App Version",
        ]
    ]

    launch_data.drop_duplicates(inplace=True)

    launch_data.rename(
        columns={
            "Event Time": "Time",
            "Event Name": "EventId",
            "Install Time":'InstallTime',
            "AppsFlyer ID": "UserAppsflyerId",
            "WIFI": "IsWifi",
            "Android ID": "UserAndroidId",
            "Customer User ID": "UserClientId",
            "Platform": "PlatformId",
            "Device Type": "DeviceId",
            "OS Version": "OsVersion",
            "App ID": "AppInternalId",
            "App Version": "AppVersion",
        },
        inplace=True,
    )

    launch_data["PlatformId"] = 2
    launch_data["AppInternalId"] = 0
    launch_data["Time"] = pd.to_datetime(launch_data["Time"])
    launch_data["InstallTime"] = pd.to_datetime(launch_data["InstallTime"])
    launch_data = launch_data.astype({"IDFA": object, "IDFV": object})

    map_dim(
        dim="DimEvent",
        data_df=launch_data,
        cat_column_df="EventId",
        cat_column_dim="EventName",
        id_column_dim="EventId",
    )

    map_dim(
        dim="DimDevice",
        data_df=launch_data,
        cat_column_df="DeviceId",
        cat_column_dim="DeviceName",
        id_column_dim="DeviceId",
    )

    dml_for_bq(
        """delete from DWH.{} where date(time)>='{}' and platformid = 2""".format(
            'FactAppLaunch', (datetime.now().date() - timedelta(days=days_back)).isoformat()
        ) )

    launches_extract_android.report_size = len(launch_data)

    launches_extract_android.target_table = 'FactAppLaunch'

    df_to_bq(
        launch_data,
        table="FactAppLaunch",
        schema=[
            bigquery.SchemaField("Time", "TIMESTAMP"),
            bigquery.SchemaField("EventId", "INTEGER"),
            bigquery.SchemaField("UserAppsflyerId", "STRING"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("UserAndroidId", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("InstallTime", "TIMESTAMP"),
            bigquery.SchemaField("UserClientId", "INTEGER"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("AppVersion", "STRING"),
            bigquery.SchemaField("OsVersion", "STRING"),
            bigquery.SchemaField("DeviceId", "INTEGER"),
            bigquery.SchemaField("IsWifi", "BOOL"),
        ],
        overwrite=False,
    )
