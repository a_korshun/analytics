def mysql_payments_android():

    import os
    import time
    import warnings
    from datetime import datetime, timedelta
    from time import gmtime, strftime
    import json

    import dask.dataframe as dd
    from dask.diagnostics import ProgressBar
    import gcsfs
    import numpy as np
    import pandas as pd
    import re
    import requests
    
    from currency_converter import CurrencyConverter

    from google.cloud import bigquery, storage
    from pandas.api.types import CategoricalDtype
    from pandas.testing import assert_frame_equal
    from tqdm import tqdm
    import pymysql
    from sqlalchemy import create_engine

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql, dml_for_bq
    from clients.appsflyer import AppsFlyer

    warnings.filterwarnings("ignore")
    
    ap = df_from_mysql(
        """
    select pp.platform 
          ,pp.user_id 
          ,Case when p.trial = 1 then 'trial' else 'regular' end as transactiontypeid 
          ,p.product_id 
          ,p.renewal_number 
          ,p.transaction_id  
          ,p.purchase_date time
          ,p.extra_data 
    from purchase p 
    left join receipt r2 on r2.id = p.receipt_id 
    left join purchase_profile pp on pp.id = r2.profile_id  
    where p.sandbox=0 and extra_data not like '%UAH%'
    and  date(purchase_date) >=date_add(current_date(), interval -2 day)
    union all 
    select pp.platform 
          ,pp.user_id 
          ,'refund' as transactiontypeid 
          ,p.product_id  
          ,p.renewal_number 
          ,p.transaction_id  
          ,p.cancellation_date time 
          ,p.extra_data 
    from purchase p 
    left join receipt r2 on r2.id = p.receipt_id 
    left join purchase_profile pp on pp.id = r2.profile_id  
    where p.sandbox=0 and p.cancellation_date is not null and p.trial!=1
    and date(purchase_date) >=date_add(current_date(), interval -2 day) and  extra_data not like '%UAH%'
    """,
        port=3312,
        schema="premiums",
    )
    
    c = CurrencyConverter()
    ap = ap[ap.extra_data!='[]'].reset_index(drop=True)
    d = pd.json_normalize(ap.extra_data.map(json.loads))
    
    ap['price'] = d.price
    ap['currency'] = d.currency
    ap["price"][ap.transactiontypeid == "trial"] = 0
    ap["price"] = ap["price"].astype(int) / 1000000

    ap.drop(columns="extra_data", inplace=True)
    
    sl = df_from_mysql('''
    select * 
    from subscriptions_list''', port=3308)
    trial_map = dict(zip(sl.bundle_name, sl.trial_days))
    subs_map = dict(zip(sl.bundle_name, sl.length))
    price_map = dict(zip(sl.bundle_name, sl.price_usd))

    ap["TrialDuration"] = ap["product_id"].map(trial_map)
    ap["SubscriptionDurationId"] = ap["product_id"].map(subs_map)
    ap["price"] = ap["product_id"].map(price_map)

    ap['price'][ap.transactiontypeid=='refund'] = 0 - ap['price']

    ids = df_from_mysql('''select id user_id
          ,device_idfa 
          ,device_idfv 
          ,device_appsflyer_id 
    from users u 
    where platform ='android' 
    and id in {} '''.format(tuple(ap[~ap.user_id.isna()]['user_id'].astype(int))))
    
    pays = ap.merge(ids, on='user_id')
    pays['AppInternalId']= 0
    pays["ProductTypeId"] = "Subscription"

    pays.rename(columns={
        "transaction_id":'TransactionId',
        'price':'UsdGross',
        'currency':'CurrencyId',
        'device_appsflyer_id':'UserAppsflyerId',
        'device_idfa':'IDFA',
        'device_idfv':'IDFV',
        'user_id':'UserClientId',
        'platform':'PlatformId',
        'renewal_number':'RebillPeriod',
        'product_id':'ProductId',
        'transactiontypeid':'TransactionTypeId',
        'time':'Time'
    }, inplace=True)

    object_columns = [
        "CurrencyId",
        "PlatformId",
        "ProductTypeId",
        "ProductId",
        "ProductTypeId", 
        "TransactionTypeId",
        "SubscriptionDurationId",
    ]

    for col in pays[object_columns]:
        pays[col] = pays[col].replace([0, -1, "None", np.nan], "Unknown")

    other_columns = np.setdiff1d(
        pays.columns.tolist(), object_columns + ["UsdGross"]  + ['AppInternalId']
    ).tolist()

    other_columns.remove("RebillPeriod")

    for col in other_columns:
        pays[col] = pays[col].replace([0, -1, "None", "Unknown", np.nan], np.nan)

    update_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )

    map_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )

    update_dim(
        data_df=pays,
        dim="DimProductType",
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )

    map_dim(
        dim="DimProductType",
        data_df=pays,
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )

    update_dim(
        data_df=pays,
        dim="DimProduct",
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )

    map_dim(
        dim="DimProduct",
        data_df=pays,
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )

    update_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )

    map_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )

    map_dim(
        dim="DimCountry",
        data_df=pays,
        cat_column_df="CurrencyId",
        cat_column_dim="CurrencyCode",
        id_column_dim="CurrencyId",
    )
    pays["CurrencyId"] = pays["CurrencyId"].astype(int)

    map_dim(
        dim="DimPlatform",
        data_df=pays,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )
    
    pays["UsdGross"] = pays["UsdGross"].astype(float)
    pays["TransactionId"] = pays["TransactionId"]
    pays["Time"] = pd.to_datetime(pays.Time)

    pays['RebillPeriod'][(pays.ProductId==31)] = pays['RebillPeriod'][(pays.ProductId==31)] - 1

    pays = pays[
        [
            "Time",
            "TransactionId",
            "UsdGross",
            "CurrencyId",
            "UserAppsflyerId",
            "IDFA",
            "IDFV",
            "UserClientId",
            "PlatformId",
            "AppInternalId",
            "SubscriptionDurationId",
            "TrialDuration",
            "RebillPeriod",
            "ProductTypeId",
            "ProductId",
            "TransactionTypeId",
        ]
    ]

    pays['UsdGross'][pays.TransactionTypeId==1] = 0
    pays['UsdGross'][(pays.ProductId==31) & (pays.RebillPeriod==1)] = 0.49
    pays['TransactionTypeId'][(pays.ProductId==31) & (pays.RebillPeriod==1)] = 1
    
    pays['AppInternalId'] = 0

    mysql_payments_android.report_size = len(pays)
    
    mysql_payments_android.target_table = 'FactTransaction'

    df_to_bq(
        pays,
        table=mysql_payments_android.target_table,
        schema=[
            bigquery.SchemaField("Time", "TIMESTAMP"),
            bigquery.SchemaField("TransactionId", "STRING"),
            bigquery.SchemaField("UsdGross", "FLOAT"),
            bigquery.SchemaField("CurrencyId", "INTEGER"),
            bigquery.SchemaField("UserAppsflyerId", "STRING"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("UserClientId", "INTEGER"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("SubscriptionDurationId", "INTEGER"),
            bigquery.SchemaField("TrialDuration", "FLOAT"),
            bigquery.SchemaField("RebillPeriod", "INTEGER"),
            bigquery.SchemaField("ProductTypeId", "INTEGER"),
            bigquery.SchemaField("ProductId", "INTEGER"),
            bigquery.SchemaField("TransactionTypeId", "INTEGER"),
        ],
        overwrite=False,
    ) 