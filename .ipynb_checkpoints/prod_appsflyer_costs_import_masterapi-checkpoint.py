def appsflyer_cost():
    
    import os
    import time
    import warnings
    from datetime import datetime, timedelta
    from time import gmtime, strftime

    import dask.dataframe as dd
    import gcsfs
    import numpy as np
    import pandas as pd
    import requests
    from dask.diagnostics import ProgressBar
    from google.cloud import bigquery, storage 
    from tqdm import tqdm

    from clients.appsflyer import AppsFlyer
    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_processing import lookup_dt
    from helpers.data_etl import df_from_bq, df_to_bq, dml_for_bq
    
    primary_token = "29d85f91-d29b-4b43-a62d-e65b0633efef"
    app_id = "id1459969523"
    af = AppsFlyer(api_token=primary_token, app_id=app_id)

    warnings.filterwarnings("ignore")
    
    geo_daily_report = pd.DataFrame()
    for d in tqdm(
        pd.date_range(
            (datetime.now().date() - timedelta(days=1)).isoformat(),
            (datetime.now().date() - timedelta(days=1)).isoformat(),
        ).tolist()
    ):
        resp = af.master_custom(
            date_from=d.strftime("%Y-%m-%d"),
            date_to=d.strftime("%Y-%m-%d"),
            as_df=True,
        )
        resp["Date"] = d
        geo_daily_report = pd.concat([geo_daily_report, resp])
    
    
    geo_daily_report.drop(
        columns=[
            "Publisher ID (af_siteid)",
            "Keywords",
            "Is Primary Attribution",
            "Conversion Rate",
        ],
        inplace=True,
    )
    
    geo_daily_report.rename(
        columns={
            "App ID": "AppId",
            "Media Source": "MediaSourceId",
            "Campaign": "CampaignInternalId",
            "Campaign ID": "CampaignId",
            "Adset": "AdSetInternalId",
            "Adset ID": "AdSetId",
            "Ad": "AdInternalId",
            "Ad ID": "AdId",
            "Channel": "ChannelId",
            "GEO": "CountryId",
            "Cost": "AdvCost",
            "Partner": "AgencyId",
        },
        inplace=True,
    )

    geo_daily_report["ClickThroughRate"] = (
        geo_daily_report["Clicks"] / geo_daily_report["Impressions"]
    )
    
    geo_daily_report = geo_daily_report[
        [
            "Date",
            "CountryId",
            "AgencyId",
            "ChannelId",
            "MediaSourceId",
            "CampaignInternalId",
            "CampaignId",
            "AdSetInternalId",
            "AdSetId",
            "AdInternalId",
            "AdId",
            "Impressions",
            "Clicks",
            "ClickThroughRate",
            "Installs",
            "AdvCost",
        ]
    ]
    
    numeric_columns = ["CampaignId", "AdSetId", "AdId"]

    metric_columns = ["Impressions", "Clicks", "ClickThroughRate", "Installs", "AdvCost"]
    for c in metric_columns:
        geo_daily_report[c] = geo_daily_report[c].replace([0, -1, "None", np.nan], 0)

    all_numeric_columns = metric_columns

    obj_columns = np.setdiff1d(
        geo_daily_report.columns.tolist(), all_numeric_columns
    ).tolist()

    for c in obj_columns:
        geo_daily_report[c] = (
            geo_daily_report[c]
            .replace([0, -1, "None", np.nan], "Unknown")
            .fillna("Unknown")
        )

    geo_daily_report["CountryId"] = geo_daily_report["CountryId"].replace(["UK", "AN"], "GB")
    geo_daily_report["CountryId"] = geo_daily_report["CountryId"].replace(
        ["NA", "No", "EU"], "Unknown"
    )

    for c in numeric_columns:
        geo_daily_report[c] = geo_daily_report[c].apply(
            lambda x: "id" + str(x) if x != "Unknown" else x
        )

    update_dim(
    data_df=geo_daily_report,
    dim="DimAgency",
    cat_column_df="AgencyId",
    cat_column_dim="AgencyName",
    id_column_dim="AgencyId",
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimChannel",
        cat_column_df="ChannelId",
        cat_column_dim="ChannelName",
        id_column_dim="ChannelId",
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimMediaSource",
        cat_column_df="MediaSourceId",
        cat_column_dim="MediaSourceName",
        id_column_dim="MediaSourceId",
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimCampaign",
        cat_column_df="CampaignInternalId",
        cat_column_dim="CampaignName",
        id_column_dim="CampaignInternalId",
        additional_columns=["CampaignId"],
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimAdSet",
        cat_column_df="AdSetInternalId",
        cat_column_dim="AdSetName",
        id_column_dim="AdSetInternalId",
        additional_columns=["AdSetId"],
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimAd",
        cat_column_df="AdInternalId",
        cat_column_dim="AdName",
        id_column_dim="AdInternalId",
        additional_columns=["AdId"],
    )
    
    map_dim(
    dim="DimAgency",
    data_df=geo_daily_report,
    cat_column_df="AgencyId",
    cat_column_dim="AgencyName",
    id_column_dim="AgencyId",
    )

    map_dim(
        data_df=geo_daily_report,
        dim="DimChannel",
        cat_column_df="ChannelId",
        cat_column_dim="ChannelName",
        id_column_dim="ChannelId",
    )

    map_dim(
        dim="DimMediaSource",
        data_df=geo_daily_report,
        cat_column_df="MediaSourceId",
        cat_column_dim="MediaSourceName",
        id_column_dim="MediaSourceId",
    )

    map_dim(
        dim="DimCampaign",
        data_df=geo_daily_report,
        cat_column_df="CampaignInternalId",
        cat_column_dim="CampaignName",
        id_column_dim="CampaignInternalId",
    )

    map_dim(
        dim="DimCountry",
        data_df=geo_daily_report,
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )

    map_dim(
        dim="DimAdSet",
        data_df=geo_daily_report,
        cat_column_df="AdSetInternalId",
        cat_column_dim="AdSetName",
        id_column_dim="AdsetInternalId",
    )

    map_dim(
        dim="DimAd",
        data_df=geo_daily_report,
        cat_column_df="AdInternalId",
        cat_column_dim="AdName",
        id_column_dim="AdInternalId",
    )

    appsflyer_cost.geo_daily_report_size = len(geo_daily_report)

    geo_daily_report["Date"] = lookup_dt(geo_daily_report["Date"])

    geo_daily_report["AdGroupId"] = geo_daily_report["AdGroupId"].astype(str)

    dml_for_bq(
        """delete from DWH.FactMarketingSpent where date>=date_add(current_date(), interval -1 day)"""
    )

    df_to_bq(
        geo_daily_report,
        dataset="DWH",
        table="FactMarketingSpent_test",
        schema=[
            bigquery.SchemaField("Date", "DATE"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("ChannelId", "INTEGER"),
            bigquery.SchemaField("AgencyId", "INTEGER"),
            bigquery.SchemaField("MediaSourceId", "INTEGER"),
            bigquery.SchemaField("CampaignInternalId", "INTEGER"),
            bigquery.SchemaField("CampaignId", "STRING"),
            bigquery.SchemaField("AdSetInternalId", "INTEGER"),
            bigquery.SchemaField("AdSetId", "STRING"),
            bigquery.SchemaField("AdInternalId", "INTEGER"),
            bigquery.SchemaField("AdId", "STRING"),
            bigquery.SchemaField("Impressions", "INTEGER"),
            bigquery.SchemaField("Clicks", "INTEGER"),
            bigquery.SchemaField("ClickThroughRate", "FLOAT"),
            bigquery.SchemaField("Installs", "INTEGER"),
            bigquery.SchemaField("AdvCost", "FLOAT"),
        ],
        overwrite=True,
    )


