def ironsrc_revenue_userlevel():

    import pandas as pd
    import numpy as np
    from tqdm import tqdm
    from datetime import datetime, timedelta
    from google.cloud import bigquery, storage

    from clients.ironsource import Ironsrc

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, dml_for_bq

    secretkey = "6dc299817f5836174e69d9a5e26d9fca"
    refreshToken = "9691bb13dd7646d59ec7bd6fa1f58ac6"

    appKeys = {
        "Nebula Android": "cc242c8d",
        "Nebula iOS": "cc2d6355",
        "Zodi iOS": "d8fde7c1",
        "Party Game":'e1918811'
    }
    appPlatform = {"cc242c8d": 2, "cc2d6355": 0, "d8fde7c1": 0, 'e1918811':0}

    irnsrc = Ironsrc(secretkey, refreshToken)

    user_level_report = pd.DataFrame()
    dates = pd.date_range(
        (datetime.now().date() - timedelta(days=3)).isoformat(),
        (datetime.now().date() - timedelta(days=1)).isoformat(),
        freq="1D",
    ).tolist()
    for d in tqdm(dates):
        for app in appKeys.values():
            try:
                report_chunk = irnsrc.detailed_report(
                    date=d.date(), report_type="user_level", appKey=app
                )
                report_chunk["IronsrcAppId"] = app
                report_chunk["Date"] = d.date()
                user_level_report = pd.concat([user_level_report, report_chunk])
            except Exception as e:
                print(e)
                pass

    conditions_idfa = [
        user_level_report.advertising_id_type == "idfa",
        user_level_report.advertising_id_type == "gaid",
    ]

    conditions_idfv = [
        user_level_report.advertising_id_type == "idfv",
    ]

    choices = [user_level_report["advertising_id"], user_level_report["advertising_id"]]

    user_level_report["IDFA"] = np.select(conditions_idfa, choices, default=np.nan)
    user_level_report["IDFV"] = np.select(conditions_idfv, [choices[0]], default=np.nan)
    user_level_report.drop(columns=["advertising_id", "advertising_id_type"], inplace=True)

    user_level_report["PlatformId"] = user_level_report["IronsrcAppId"].map(appPlatform)
    
    app_map = {
        "cc242c8d":0,
        "cc2d6355":0,
        "d8fde7c1":4,
        "e1918811":5,
    }

    user_level_report['AppInternalId'] = user_level_report["IronsrcAppId"].map(app_map)

    user_level_report.rename(
        columns={
            "ad_unit": "InAppAdTypeId",
            "user_id": "UserId", 
            "ad_network": "InAppAdNetworkId",
            "segment": "InAppAdSegmentId",
            "placement": "InAppAdPlacementId",
            "impressions": "Impressions",
            "revenue": "Revenue",
            "AB_Testing": "AbTestGroup",
        },
        inplace=True,
    )

    user_level_report = user_level_report[
        [
            "Date",
            "UserId",
            "IDFA",
            "IDFV",
            "PlatformId",
            "IronsrcAppId",
            "InAppAdSegmentId",
            "InAppAdTypeId",
            "InAppAdPlacementId",
            "InAppAdNetworkId",
            "AbTestGroup",
            "Impressions",
            "Revenue",
            "AppInternalId"
        ]
    ]

    object_columns = [
        "IronsrcAppId",
        "InAppAdSegmentId",
        "InAppAdTypeId",
        "InAppAdPlacementId",
        "InAppAdNetworkId",
        "AbTestGroup",
    ]

    for col in object_columns:
        user_level_report[col] = user_level_report[col].replace([0, -1, "None", np.nan], "Unknown")
        
    numeric_columns = [
        "Impressions",
        "Revenue",
    ]

    for col in numeric_columns:
        user_level_report[col] = user_level_report[col].replace([0, -1, "None", np.nan, "Unknown"], 0)
        
    update_dim(
        data_df=user_level_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    map_dim(
        data_df=user_level_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    update_dim(
        data_df=user_level_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )

    map_dim(
        data_df=user_level_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )

    update_dim(
        data_df=user_level_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )

    map_dim(
        data_df=user_level_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )

    update_dim(
        data_df=user_level_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )

    map_dim(
        data_df=user_level_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )

    user_level_report["Date"] = pd.to_datetime(user_level_report["Date"])
    
    ironsrc_revenue_userlevel.report_size = len(user_level_report)
    
    ironsrc_revenue_userlevel.target_table = 'FactAdRevenueUserLevel'

    df_to_bq(
        user_level_report,
        table=ironsrc_revenue_userlevel.target_table,
        schema=[
            bigquery.SchemaField("Date", "Date"),
            bigquery.SchemaField("UserId", "STRING"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("InAppAdSegmentId", "INTEGER"),
            bigquery.SchemaField("IronsrcAppId", "STRING"),
            bigquery.SchemaField("InAppAdTypeId", "INTEGER"),
            bigquery.SchemaField("InAppAdPlacementId", "INTEGER"),
            bigquery.SchemaField("InAppAdNetworkId", "INTEGER"),
            bigquery.SchemaField("AbTestGroup", "STRING"),
            bigquery.SchemaField("Impressions", "INTEGER"),
            bigquery.SchemaField("Revenue", "FLOAT"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
        ],
        overwrite=False,
    )
    
    dedup_query = """
    create or replace table DWH.FactAdRevenueUserLevel  as
    select * except(rn) from(
    select *, row_number() over (partition by Date, IDFA, IDFV, InAppAdSegmentId, IronsrcAppId, InAppAdTypeId, InAppAdPlacementId, InAppAdNetworkId,AbTestGroup, appinternalid ) rn
    from DWH.FactAdRevenueUserLevel) where rn=1 
    """
    
    dml_for_bq(dedup_query, output=True)