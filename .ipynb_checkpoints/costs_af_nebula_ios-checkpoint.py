def appsflyer_cost():
    
    import os
    import time
    import warnings
    from datetime import datetime, timedelta
    from time import gmtime, strftime

    import dask.dataframe as dd
    import gcsfs
    import numpy as np
    import pandas as pd
    import requests
    from dask.diagnostics import ProgressBar
    from google.cloud import bigquery, storage
    from pandas.api.types import CategoricalDtype
    from pandas.testing import assert_frame_equal
    from tqdm import tqdm

    from clients.appsflyer import AppsFlyer
    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_processing import lookup_dt
    from helpers.data_etl import df_from_bq, df_to_bq, dml_for_bq

    primary_token = "29d85f91-d29b-4b43-a62d-e65b0633efef"
    app_id = "id1459969523"
    af = AppsFlyer(api_token=primary_token, app_id=app_id)

    warnings.filterwarnings("ignore")

    geo_daily_report = pd.DataFrame()
    max_tries = 10
    for d in tqdm(
        pd.date_range(
            (datetime.now().date() - timedelta(days=3)).isoformat(),
            (datetime.now().date() - timedelta(days=1)).isoformat(),
        ).tolist()
    ):
        for i in range(max_tries):
            try:
                if i <= 3:
                    time.sleep(0.3) 
                else: 
                    time.sleep(25)
                resp = af.master_custom(
                    date_from=d.strftime("%Y-%m-%d"),
                    date_to=d.strftime("%Y-%m-%d"),
                    as_df=True,
                )
                geo_daily_report = pd.concat([geo_daily_report, resp])
                break
            except Exception:
                print('{} try on date {} failed. Retrying...'.format(i,d))
                continue

    geo_daily_report.rename(
        columns={
            "App ID": "AppId",
            "Media Source": "MediaSourceId",
            "Campaign": "CampaignNameShort",
            "Campaign ID": "CampaignId",
            "Adset": "AdSetNameShort",
            "Adset ID": "AdSetId",
            "Ad": "AdNameShort",
            "Ad ID": "AdId",
            "Install Time": "Date",
            "GEO": "CountryId",
            "Cost": "AdvCost",
            "Partner": "AgencyId",
            #"Publisher ID (af_siteid)": "PublisherInternalId",
        },
        inplace=True,
    )

    metric_columns = ["Impressions", "Clicks", "Installs", "AdvCost"]
    for c in metric_columns:
        geo_daily_report[c] = geo_daily_report[c].replace([0, -1, "None", np.nan], 0)

    obj_columns = np.setdiff1d(geo_daily_report.columns.tolist(), metric_columns).tolist()

    for c in obj_columns:
        geo_daily_report[c] = (
            geo_daily_report[c]
            .replace([0, -1, "None", np.nan, " "], "Unknown")
            .fillna("Unknown")
            .apply(lambda x: str(x).rstrip(" "))
        )

    numeric_columns = ["CampaignId"
                       , "AdSetId"
                       , "AdId"
                       #, "PublisherInternalId"
                       ]

    for c in numeric_columns:
        geo_daily_report[c] = geo_daily_report[c].apply(
            lambda x: "id" + str(x) if x != "Unknown" else x
        )

    dim_columns = [
        ["CampaignInternalId", "CampaignId", "CampaignNameShort"],
        ["AdSetInternalId", "AdSetId", "AdSetNameShort"],
        ["AdInternalId", "AdId", "AdNameShort"],
    ]

    for c in dim_columns:
        geo_daily_report[c[0]] = (
            geo_daily_report[c[1]].astype(str) + " - " + geo_daily_report[c[2]].astype(str)
        ).replace("Unknown - Unknown", "Unknown")

    geo_daily_report["CountryId"] = geo_daily_report["CountryId"].replace(
        ["UK", "AN"], "GB"
    )
    geo_daily_report["CountryId"] = geo_daily_report["CountryId"].replace(
        ["NA", "No", "EU", " ", "", "XX", "ZZ"], "Unknown"
    )

        
    update_dim(
        data_df=geo_daily_report,
        dim="DimAgency",
        cat_column_df="AgencyId",
        cat_column_dim="AgencyName",
        id_column_dim="AgencyId",
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimMediaSource",
        cat_column_df="MediaSourceId",
        cat_column_dim="MediaSourceName",
        id_column_dim="MediaSourceId",
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimCampaign",
        cat_column_df="CampaignInternalId",
        cat_column_dim="CampaignName",
        id_column_dim="CampaignInternalId",
        additional_columns=["CampaignId","CampaignNameShort"],
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimAdSet",
        cat_column_df="AdSetInternalId",
        cat_column_dim="AdSetName",
        id_column_dim="AdSetInternalId",
        additional_columns=["AdSetId","AdSetNameShort"],
    )

    update_dim(
        data_df=geo_daily_report,
        dim="DimAd",
        cat_column_df="AdInternalId",
        cat_column_dim="AdName",
        id_column_dim="AdInternalId",
        additional_columns=["AdId","AdNameShort"],
    )
    
    # update_dim(
    #     data_df=geo_daily_report,
    #     dim="DimPublisher",
    #     cat_column_df="PublisherInternalId",
    #     cat_column_dim="PublisherId",
    #     id_column_dim="PublisherInternalId",
    # )
    
    map_dim(
        dim="DimAgency",
        data_df=geo_daily_report,
        cat_column_df="AgencyId",
        cat_column_dim="AgencyName",
        id_column_dim="AgencyId",
    )

    map_dim(
        dim="DimMediaSource",
        data_df=geo_daily_report,
        cat_column_df="MediaSourceId",
        cat_column_dim="MediaSourceName",
        id_column_dim="MediaSourceId",
    )

    map_dim(
        dim="DimCampaign",
        data_df=geo_daily_report,
        cat_column_df="CampaignInternalId",
        cat_column_dim="CampaignName",
        id_column_dim="CampaignInternalId",
    )

    map_dim(
        dim="DimCountry",
        data_df=geo_daily_report,
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )

    map_dim(
        dim="DimAdSet",
        data_df=geo_daily_report,
        cat_column_df="AdSetInternalId",
        cat_column_dim="AdSetName",
        id_column_dim="AdsetInternalId",
    )

    map_dim(
        dim="DimAd",
        data_df=geo_daily_report,
        cat_column_df="AdInternalId",
        cat_column_dim="AdName",
        id_column_dim="AdInternalId",
    )
    
    # map_dim(
    #     data_df=geo_daily_report,
    #     dim="DimPublisher",
    #     cat_column_df="PublisherInternalId",
    #     cat_column_dim="PublisherId",
    #     id_column_dim="PublisherInternalId",
    # )

    geo_daily_report["Date"] = lookup_dt(geo_daily_report["Date"])

    geo_daily_report["PlatformId"] = 0  # iOS
    geo_daily_report["AppInternalId"] = 0

    geo_daily_report["ClickThroughRate"] = 0
    geo_daily_report.reset_index(inplace=True, drop=True)
    geo_daily_report["ClickThroughRate"][geo_daily_report["Impressions"]>0] = (
        geo_daily_report["Clicks"] / geo_daily_report["Impressions"]
    ).round(4)

    geo_daily_report = geo_daily_report[
        [
            "Date",
            "AppInternalId",
            "PlatformId",
            "CountryId",
            "AgencyId",
            "MediaSourceId",
            "CampaignInternalId",
            #"PublisherInternalId",
            "AdSetInternalId",
            "AdInternalId",
            "Impressions",
            "Clicks",
            "Installs",
            "AdvCost",
            "ClickThroughRate",
        ]
    ]
    
    geo_daily_report['MediaSourceId'][geo_daily_report['MediaSourceId']==8] = -1

    appsflyer_cost.report_size = len(geo_daily_report)
    
    appsflyer_cost.target_table = 'FactMarketingSpent'
    
    dml_for_bq('''delete from DWH.{} where date>='{}' and platformid = 0 and appinternalid = 0'''.format(appsflyer_cost.target_table,
                                                              (datetime.now().date() - timedelta(days=3)).isoformat()))

    df_to_bq(
        geo_daily_report,
        dataset="DWH",
        table=appsflyer_cost.target_table,
        schema=[
            bigquery.SchemaField("Date", "DATE"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("AgencyId", "INTEGER"),
            bigquery.SchemaField("MediaSourceId", "INTEGER"),
            bigquery.SchemaField("CampaignInternalId", "INTEGER"),
            #bigquery.SchemaField("PublisherInternalId", "INTEGER"),
            bigquery.SchemaField("AdSetInternalId", "INTEGER"),
            bigquery.SchemaField("AdInternalId", "INTEGER"),
            bigquery.SchemaField("Impressions", "INTEGER"),
            bigquery.SchemaField("Clicks", "INTEGER"),
            bigquery.SchemaField("ClickThroughRate", "FLOAT"),
            bigquery.SchemaField("Installs", "INTEGER"),
            bigquery.SchemaField("AdvCost", "FLOAT"),
        ],
        overwrite=False,
    )