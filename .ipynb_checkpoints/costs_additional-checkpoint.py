def additional_costs():
    
    import pandas as pd
    import gspread
    import json
    import numpy as np
    import datetime
    from tqdm import tqdm
    from googleapiclient.discovery import build
    from google.oauth2 import service_account
    from google.cloud import bigquery
    from currency_converter import CurrencyConverter

    from helpers.data_etl import df_from_bq, df_to_bq
    from helpers.dimensions import map_dim, update_dim

    c = CurrencyConverter(fallback_on_missing_rate=True, fallback_on_wrong_date=True)

    key = "key.json"
    scopes = ["https://www.googleapis.com/auth/drive"]
    credentials = service_account.Credentials.from_service_account_file(key, scopes=scopes)
    service = build("sheets", "v4", credentials=credentials)
    gc = gspread.authorize(credentials)

    sheet_id = "1hT-Qi-9amOLCIaTqPUnCg6Tddo6GWf_sMVkPcNRmJks"

    wks = gc.open_by_key(sheet_id)
    sheet_metadata = service.spreadsheets().get(spreadsheetId=sheet_id).execute()
    properties = sheet_metadata.get("sheets")
    sheets = []
    sheets_to_exclude = ["MediasSource AF Names", "CountryCodes"]
    df_agencies = pd.DataFrame()
    df_mediasources = pd.DataFrame()

    for item in tqdm(properties):
        title = item.get("properties").get("title")
        sheets.append(title)
        if item.get("properties").get("title") not in sheets_to_exclude:
            if title.endswith("MS"):
                temp_df = pd.DataFrame(
                    wks.get_worksheet(sheets.index(title)).get_all_records()
                )
                temp_df["sheet_title"] = title.rstrip(". MS")
                df_mediasources = pd.concat([df_mediasources, temp_df])
            elif title.endswith("A"):
                temp_df = pd.DataFrame(
                    wks.get_worksheet(sheets.index(title)).get_all_records()
                )
                temp_df["sheet_title"] = title.rstrip(". A")
                df_agencies = pd.concat([df_agencies, temp_df])

    df_agencies.reset_index(inplace=True, drop=True)
    df_mediasources.reset_index(inplace=True, drop=True)

    def remove_spaces(df):
        for c in df.columns:
            df[c] = df[c].replace(["", " "], np.nan)


    def convert_to_usd(df):
        for i, r in df.iterrows():
            if df.loc[i, "currency"] == "EUR":
                df.loc[i, "spend"] = c.convert(
                    df.loc[i, "spend"], "EUR", "USD", date=pd.to_datetime(df.loc[i, "date"])
                )


    platform_map = {"ios": 0, "android": 2}

    df_list = [df_agencies, df_mediasources]
    for df in df_list:
        remove_spaces(df)
        convert_to_usd(df)
        df["platform"] = df["platform"].str.lower().map(platform_map)
        df["date"] = pd.to_datetime(df["date"]).apply(lambda x: x.strftime("%Y-%m-%d"))

        df.rename(
            columns={
                "af_source": "MediaSourceId",
                "af_campaign": "CampaignNameShort",
                "country": "CountryId",
            },
            inplace=True,
        )

        campaign_clmns = ["CampaignNameShort", "CampaignId"]
        for clmn in campaign_clmns:
            if clmn not in df.columns.tolist():
                df[clmn] = "Unknown"
        df["CampaignNameShort"] = (
            df["CampaignNameShort"]
            .replace([0, -1, "None", np.nan, " "], "Unknown")
            .fillna("Unknown")
            .apply(lambda x: str(x).rstrip(" "))
        )
        df["MediaSourceId"] = (
            df["MediaSourceId"].fillna("Unknown").apply(lambda x: str(x).rstrip(" "))
        )

        dim_columns = ["CampaignInternalId", "CampaignId", "CampaignNameShort"]

        df[dim_columns[0]] = (
            df[dim_columns[1]].astype(str) + " - " + df[dim_columns[2]].astype(str)
        ).replace("Unknown - Unknown", "Unknown")

        update_dim(
            data_df=df,
            dim="DimMediaSource",
            cat_column_df="MediaSourceId",
            cat_column_dim="MediaSourceName",
            id_column_dim="MediaSourceId",
        )

        update_dim(
            data_df=df,
            dim="DimCampaign",
            cat_column_df="CampaignInternalId",
            cat_column_dim="CampaignName",
            id_column_dim="CampaignInternalId",
            additional_columns=["CampaignId", "CampaignNameShort"],
        )

        map_dim(
            data_df=df,
            dim="DimMediaSource",
            cat_column_df="MediaSourceId",
            cat_column_dim="MediaSourceName",
            id_column_dim="MediaSourceId",
        )

        map_dim(
            data_df=df,
            dim="DimCampaign",
            cat_column_df="CampaignInternalId",
            cat_column_dim="CampaignName",
            id_column_dim="CampaignInternalId",
        )

        df.drop(
            columns=[
                c
                for c in [
                    "sheet_title",
                    "currency",
                    "mediaSource",
                    "CampaignId",
                    "CampaignNameShort",
                    "countryname",
                ]
                if c in df.columns.tolist()
            ],
            inplace=True,
        )

        map_dim(
            dim="DimCountry",
            data_df=df,
            cat_column_df="CountryId",
            cat_column_dim="CountryCodeShort",
            id_column_dim="CountryId",
        )

        df.rename(
            columns={
                "date": "Date",
                "platform": "PlatformId",
                "cpa": "CPA",
                "spend": "AdvCost",
            },
            inplace=True,
        )

        df["CountryId"] = df["CountryId"].astype(int)

    bq_data = df_from_bq(
        """
    with raw as (select date(installtime) Date
                      ,PlatformId
                      ,MediaSourceId
                      ,CampaignInternalId
                      ,CountryId
                      ,du.UserAppsFlyerId
                      ,case when du.userappsflyerid = ft.userappsflyerid then 1 else 0 end Trials
                from DWH.DimUser du
                left join (select userappsflyerid
                            ,productid
                      from DWH.FactTransaction ft
                      where transactiontypeid = 1) ft
                      on ft.userappsflyerid = du.userappsflyerid
                where du.mediasourceid in {}
                )
    select Date
          ,PlatformId
          ,r.MediaSourceId
          --,r.CampaignInternalId
          --,CampaignName
          --,MediaSourceName
          ,CountryId
          ,Count(distinct userappsflyerid) Installs
          ,sum(Trials) Trials
    from raw r
    join DWH.DimMediaSource d on d.mediasourceid = r.mediasourceid
    --join DWH.DimCampaign dd on dd.campaigninternalid = r.campaigninternalid
    group by 1,2,3,4--,5,6,7
    """.format(
            tuple(
                pd.concat(
                    [
                        pd.Series(df_agencies["MediaSourceId"].unique()),
                        pd.Series(df_mediasources["MediaSourceId"].unique()),
                    ]
                )
            )
        ),
        dtype={"AdvCost": "float64", "ClickThroughRate": "float64"},
    )

    bq_df = (
        bq_data.groupby(["Date", "PlatformId", "MediaSourceId", "CountryId"])
        .agg({"Installs": "sum", "Trials": "sum"})
        .reset_index()
    )

    dfm = (
        df_mediasources.groupby(["Date", "PlatformId", "CountryId", "MediaSourceId"])
        .agg({"AdvCost": "sum"})
        .reset_index()
        .merge(bq_df, on=["Date", "PlatformId", "CountryId", "MediaSourceId"], how="left")
        .fillna(0)
    )

    dfa = (
        df_agencies.groupby(["Date", "PlatformId", "CountryId", "MediaSourceId"])
        .agg({"CPA": "mean"})
        .reset_index()
        .merge(bq_df[(bq_df['MediaSourceId'].isin(df_agencies.MediaSourceId)) & (bq_df.Date>=df_agencies.Date.min()) & (bq_df.CountryId==840)], on=["Date", "PlatformId", "CountryId", "MediaSourceId"], how='right')
        .sort_values(by=['MediaSourceId','Date'])
        .fillna(method='ffill')
        .fillna(method='bfill') 
    )

    dfa["AdvCost"] = dfa["CPA"] * dfa["Trials"]
    dfa.drop(columns="CPA", inplace=True)

    costs_df = pd.concat([dfa, dfm])
    costs_df['Date'] = pd.to_datetime(costs_df['Date'])
    costs_df.AdvCost = costs_df.AdvCost.astype(float)

    additional_costs.report_size = len(costs_df)

    additional_costs.target_table = "FactMarketingAdditionalSpent"

    df_to_bq(
        costs_df,
        dataset="DWH",
        table=additional_costs.target_table,
        schema=[
            bigquery.SchemaField("Date", "Date"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("Installs", "FLOAT"),
            bigquery.SchemaField("Trials", "FLOAT"),
            bigquery.SchemaField("AdvCost", "FLOAT")
        ],
        overwrite=True,
    )

