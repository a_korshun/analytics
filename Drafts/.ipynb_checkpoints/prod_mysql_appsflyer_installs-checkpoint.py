def mysql_appsflyer_installs():
    from sqlalchemy import create_engine
    import pymysql
    import pandas as pd
    import numpy as np
    import gcsfs
    from dask.diagnostics import ProgressBar
    from dask import dataframe as dd
    from google.cloud import bigquery, storage
    from time import gmtime, strftime

    from tqdm import tqdm

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql, dml_for_bq
    from clients.appsflyer import AppsFlyer

    import warnings
    warnings.filterwarnings("ignore")
    
    installs = df_from_mysql(
            """select id,
                    appsflyer_id,
                    attributed_touch_time,
                    attributed_touch_type,
                    install_time,
                    partner,
                    media_source,
                    channel,
                    campaign,
                    campaign_id,
                    adset,
                    adset_id,
                    ad,
                    ad_id,
                    country_code,
                    ip,
                    wifi,
                    language,
                    idfa,
                    idfv,
                    platform,
                    device_type,
                    os_version,
                    app_version,
                    app_id,
                    app_name,
                    is_retargeting 
                from apps_flyer_installs 
            where date(install_time) >= date_add(current_date(), interval -1 day) and date(install_time)<date(current_date)""",
            output=False,
        )

    dimuser = installs.copy()

    dimuser.rename(
        columns={
            "id":"UserClientId",
            "appsflyer_id": "UserAppsflyerId",
            "attributed_touch_time": "AttributionEventTime",
            "attributed_touch_type": "AttributionEventTypeId",
            "install_time": "InstallTime",
            "partner": "AgencyId",
            "media_source": "MediaSourceId",
            "channel": "ChannelId",
            "campaign": "CampaignInternalId",
            "campaign_id": "CampaignId",
            "adset": "AdSetInternalId",
            "adset_id": "AdSetId",
            "ad": "AdInternalId",
            "ad_id": "AdId",
            "country_code": "CountryId",
            "ip": "RegistrationIp",
            "wifi": "IsWifiRegistration",
            "language": "LanguageId",
            "idfa": "IDFA",
            "idfv": "IDFV",
            "platform": "PlatformId",
            "device_type": "DeviceId",
            "os_version": "OsVersion",
            "app_version": "AppVersion",
            "app_id": "AppId",
            "app_name": "AppInternalId",
            "is_retargeting": "IsRetargeting",
        },
        inplace=True,
    )
    
    dimuser["CountryId"] = dimuser["CountryId"].replace(["UK", "AN"], "GB")
    dimuser["CountryId"] = dimuser["CountryId"].replace(['NA', 'No','EU', " ", ""], "Unknown")

    obj_columns = [
        "UserAppsflyerId",
        "AttributionEventTypeId",
        "AgencyId",
        "MediaSourceId",
        "ChannelId",
        "CampaignInternalId", 
        "AdSetInternalId", 
        "AdInternalId", 
        "CountryId",
        "LanguageId",
        "PlatformId",
        "DeviceId",
        "AppId",
        "AppInternalId",
    ]

    for col in obj_columns:
        dimuser[col] = dimuser[col].replace([0, -1, "None", np.nan, " ", ""], "Unknown")

    numeric_columns = ["CampaignId", "AdSetId", "AdId"]
    for c in numeric_columns:
        dimuser[c] = dimuser[c].replace([0, -1, "None", np.nan, " ", ""], 'Unknown').fillna("Unknown")

    for c in numeric_columns:
        dimuser[c] = dimuser[c].apply(
            lambda x: "id" + str(x) if x != "Unknown" else x
        )

    other_columns = np.setdiff1d(dimuser.columns.tolist(), obj_columns+numeric_columns).tolist()

    for col in other_columns:
        dimuser[col] = dimuser[col].replace([0, -1, "None", "Unknown", np.nan, " ", ""], np.nan)

    update_dim(
        data_df=dimuser,
        dim="DimCampaign",
        cat_column_df="CampaignInternalId",
        cat_column_dim="CampaignName",
        id_column_dim="CampaignInternalId",
        additional_columns=["CampaignId"],
    )

    update_dim(
        data_df=dimuser,
        dim="DimAdSet",
        cat_column_df="AdSetInternalId",
        cat_column_dim="AdSetName",
        id_column_dim="AdSetInternalId",
        additional_columns=["AdSetId"],
    )

    update_dim(
        data_df=dimuser,
        dim="DimAgency",
        cat_column_df="AgencyId",
        cat_column_dim="AgencyName",
        id_column_dim="AgencyId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimMediaSource",
        cat_column_df="MediaSourceId",
        cat_column_dim="MediaSourceName",
        id_column_dim="MediaSourceId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimApp",
        cat_column_df="AppInternalId",
        cat_column_dim="AppName",
        id_column_dim="AppInternalId",
        additional_columns=['AppId']
    )

    update_dim(
        data_df=dimuser,
        dim="DimDevice",
        cat_column_df="DeviceId",
        cat_column_dim="DeviceName",
        id_column_dim="DeviceId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimPlatform",
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimLanguage",
        cat_column_df="LanguageId",
        cat_column_dim="LanguageName",
        id_column_dim="LanguageId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimAd",
        cat_column_df="AdInternalId",
        cat_column_dim="AdName",
        id_column_dim="AdInternalId",
        additional_columns=["AdId"],
    )

    update_dim(
        data_df=dimuser,
        dim="DimChannel",
        cat_column_df="ChannelId",
        cat_column_dim="ChannelName",
        id_column_dim="ChannelId",
    )

    update_dim(
        data_df=dimuser,
        dim="DimAttributionEventType",
        cat_column_df="AttributionEventTypeId",
        cat_column_dim="AttributionEventTypeName",
        id_column_dim="AttributionEventTypeId",
    )

    map_dim(
        dim="DimApp",
        data_df=dimuser,
        cat_column_df="AppInternalId",
        cat_column_dim="AppName",
        id_column_dim="AppInternalId",
    )

    map_dim(
        dim="DimDevice",
        data_df=dimuser,
        cat_column_df="DeviceId",
        cat_column_dim="DeviceName",
        id_column_dim="DeviceId",
    )

    map_dim(
        dim="DimPlatform",
        data_df=dimuser,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )

    map_dim(
        dim="DimLanguage",
        data_df=dimuser,
        cat_column_df="LanguageId",
        cat_column_dim="LanguageName",
        id_column_dim="LanguageId",
    )

    map_dim(
        dim="DimCountry",
        data_df=dimuser,
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )

    map_dim(
        dim="DimAd",
        data_df=dimuser,
        cat_column_df="AdInternalId",
        cat_column_dim="AdName",
        id_column_dim="AdInternalId",
    )

    map_dim(
        dim="DimAdSet",
        data_df=dimuser,
        cat_column_df="AdSetInternalId",
        cat_column_dim="AdSetName",
        id_column_dim="AdSetInternalId",
    )

    map_dim(
        dim="DimCampaign",
        data_df=dimuser,
        cat_column_df="CampaignInternalId",
        cat_column_dim="CampaignName",
        id_column_dim="CampaignInternalId",
    )

    map_dim(
        dim="DimChannel",
        data_df=dimuser,
        cat_column_df="ChannelId",
        cat_column_dim="ChannelName",
        id_column_dim="ChannelId",
    )

    map_dim(
        dim="DimMediaSource",
        data_df=dimuser,
        cat_column_df="MediaSourceId",
        cat_column_dim="MediaSourceName",
        id_column_dim="MediaSourceId",
    )

    map_dim(
        dim="DimAttributionEventType",
        data_df=dimuser,
        cat_column_df="AttributionEventTypeId",
        cat_column_dim="AttributionEventTypeName",
        id_column_dim="AttributionEventTypeId",
    )

    map_dim(
        dim="DimAgency",
        data_df=dimuser,
        cat_column_df="AgencyId",
        cat_column_dim="AgencyName",
        id_column_dim="AgencyId",
    )
    
    dimuser['IsRetargeting'] = dimuser['IsRetargeting'].replace({'false':0,'False':0,'true':1,'True':1}) 
    dimuser['IsWifiRegistration'] = dimuser['IsWifiRegistration'].replace({'false':0,'False':0,'true':1,'True':1}) 

    dimuser['IsRetargeting'] = dimuser['IsRetargeting'].astype(bool)
    dimuser['IsWifiRegistration'] = dimuser['IsWifiRegistration'].astype(bool)
    
    mysql_appsflyer_installs.dimuser_size = len(dimuser)

    df_to_bq(
        dimuser,
        table="DimUser",
        schema=[
            bigquery.SchemaField("UserClientId", "INTEGER"),
            bigquery.SchemaField("UserAppsflyerId", "STRING"),
            bigquery.SchemaField("AttributionEventTime", "TIMESTAMP"),
            bigquery.SchemaField("AttributionEventTypeId", "INTEGER"),
            bigquery.SchemaField("InstallTime", "TIMESTAMP"),
            bigquery.SchemaField("AgencyId", "INTEGER"),
            bigquery.SchemaField("MediaSourceId", "INTEGER"),
            bigquery.SchemaField("ChannelId", "INTEGER"),
            bigquery.SchemaField("CampaignInternalId", "INTEGER"),
            bigquery.SchemaField("CampaignId", "STRING"),
            bigquery.SchemaField("AdSetInternalId", "INTEGER"),
            bigquery.SchemaField("AdSetId", "STRING"),
            bigquery.SchemaField("AdInternalId", "INTEGER"),
            bigquery.SchemaField("AdId", "STRING"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("RegistrationIp", "STRING"),
            bigquery.SchemaField("IsWifiRegistration", "BOOLEAN"),
            bigquery.SchemaField("LanguageId", "INTEGER"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("DeviceId", "INTEGER"),
            bigquery.SchemaField("OsVersion", "STRING"),
            bigquery.SchemaField("AppVersion", "STRING"),
            bigquery.SchemaField("AppId", "STRING"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("IsRetargeting", "BOOLEAN"),
        ],
        overwrite=False,
    ) 