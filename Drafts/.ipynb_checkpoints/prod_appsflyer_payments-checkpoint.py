def appsflyer_payments():

    import os
    import time
    import warnings
    from datetime import datetime, timedelta
    from time import gmtime, strftime
    import json

    import dask.dataframe as dd
    from dask.diagnostics import ProgressBar
    import gcsfs
    import numpy as np
    import pandas as pd
    import re
    import requests

    from google.cloud import bigquery, storage 
    from tqdm import tqdm 

    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq
    from clients.appsflyer import AppsFlyer

    warnings.filterwarnings("ignore")

    primary_token = "29d85f91-d29b-4b43-a62d-e65b0633efef"
    app_id = "id1459969523"
    af = AppsFlyer(api_token=primary_token, app_id=app_id) 
    
    events = "billing_subscription_success,back_billing_refund_subscription,billing_subscription_trial_success,iap_purchase_success"
    
    iaps = pd.DataFrame()
    for d in tqdm(
        pd.date_range(
                (datetime.now().date() - timedelta(days=2)).isoformat(),
                (datetime.now().date() - timedelta(days=1)).isoformat(),
                freq="1D",
            ).tolist()
        ):
        resp = af.organic_in_app_events_report(
            date_from=d.strftime("%Y-%m-%d"),
            date_to=d.strftime("%Y-%m-%d"),
            as_df=True,
            event_name=events,
        )

        iaps = pd.concat([iaps, resp])
            
    pays = iaps_organic.copy()
    pays.reset_index(drop=True, inplace=True)
    pays = pays[
        [
            "Event Time",
            "Event Name",
            "Event Value",
            "Event Revenue",
            "Event Revenue Currency",
            "Event Revenue Preferred",
            "Event Source",
            "Country Code",
            "AppsFlyer ID",
            "IDFA",
            "IDFV",
            "Customer User ID",
            "Platform",
            "App Name",
        ]
    ]

    pays["Country Code"] = pays["Country Code"].replace(["UK", "AN"], "GB")
    pays["Country Code"] = pays["Country Code"].replace(["NA", "No", "EU"], "Unknown")

    pp = pd.json_normalize(pays["Event Value"].map(json.loads))
    if "product_id" not in pp.columns:
        pp["product_id"] = np.nan
    
    pp["ProductId"] = pp.af_content_id.combine_first(pp.subscription_id).combine_first(
        pp.product_id
    )
    
    pp["time"] = pp.time.combine_first(pp.refunded_at)
    pp["time"] = pp["time"].apply(
        lambda x: x if str(x) == "nan" else datetime.fromtimestamp(x)
    )
    
    pays = pays.join(pp)

    inapp = pays[pays["Event Name"] == "iap_purchase_success"]
    pays = pays[pays["Event Name"] != "iap_purchase_success"]

    iapp_list = inapp[inapp["Event Name"] == "iap_purchase_success"]["ProductId"].unique()
    iapp_price_map = dict.fromkeys(iapp_list)
    p = "[\d]+[.,\d]+|[\d]*[.][\d]+|[\d]+"
    for val in iapp_list:
        if re.search(p, val) is not None:
            for catch in re.finditer(p, val):
                iapp_price_map[val] = catch[0]

    inapp["Event Revenue"] = inapp["ProductId"].map(iapp_price_map)

    pays["Event Revenue"].fillna(0, inplace=True)

    pays = pd.concat([pays, inapp])
    
    pays["Event Time"] = pd.to_datetime(pays["Event Time"])
    pays["Event Time"][~pays.time.isna()] = pd.to_datetime(pays["time"])

    pays.drop(
        columns=[
            "af_content_id",
            "subscription_id",
            "product_id",
            "af_revenue",
            "af_quantity",
            "Event Value",
            "Event Revenue Preferred",
            "Event Source",
            "amount_usd",
            "refunded_at",
            "time",
        ],
        inplace=True,
    )

    pays["af_content_type"] = pays["Event Name"].apply(
        lambda x: "InApp" if x == "iap_purchase_success" else "Subscription"
    )

    pays.rename(
        columns={
            "Event Time": "Time",
            "Event Name": "EventId",
            "Event Revenue": "UsdGross",
            "Event Revenue Currency": "CurrencyId",
            "Country Code": "CountryId",
            "AppsFlyer ID": "UserAppsflyerId",
            "Customer User Id": "UserClientId",
            "Customer User ID": "UserClientId",
            "Platform": "PlatformId",
            "App Name": "AppInternalId",
            "af_content_type": "ProductTypeId",
            "subscription_type": "SubscriptionDurationId",
            "rebill_period": "RebillPeriod",
            "trial_duration": "TrialDuration",
            "transaction_id": "TransactionId",
        },
        inplace=True,
    )

    pays["CountryId"] = pays["CountryId"].replace("UK", "GB")

    event_type = {
        "billing_subscription_success": "regular",
        "iap_purchase_success": "regular",
        "billing_subscription_trial_success": "trial",
        "back_billing_refund_subscription": "refund",
    }
    pays["TransactionTypeId"] = pays["EventId"].map(event_type)

    object_columns = [
        "CurrencyId",
        "CountryId",
        "PlatformId",
        "ProductTypeId",
        "ProductId",
        "ProductTypeId",
        "AppInternalId",
        "TransactionTypeId",
        "SubscriptionDurationId",
    ]

    for col in pays[object_columns]:
        pays[col] = pays[col].replace([0, -1, "None", np.nan], "Unknown")

    other_columns = np.setdiff1d(
        pays.columns.tolist(), object_columns + ["UsdGross"]
    ).tolist()

    other_columns.remove("RebillPeriod")

    for col in other_columns:
        pays[col] = pays[col].replace([0, -1, "None", "Unknown", np.nan], np.nan)

    update_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )

    map_dim(
        data_df=pays,
        dim="DimSubscriptionDuration",
        cat_column_df="SubscriptionDurationId",
        cat_column_dim="SubscriptionDurationName",
        id_column_dim="SubscriptionDurationId",
    )
    
    update_dim(
        data_df=pays,
        dim="DimEvent",
        cat_column_df="EventId",
        cat_column_dim="EventName",
        id_column_dim="EventId",
    )

    map_dim(
        dim="DimEvent",
        data_df=pays,
        cat_column_df="EventId",
        cat_column_dim="EventName",
        id_column_dim="EventId",
    )
    
    update_dim(
        data_df=pays,
        dim="DimProductType",
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )

    map_dim(
        dim="DimProductType",
        data_df=pays,
        cat_column_df="ProductTypeId",
        cat_column_dim="ProductTypeName",
        id_column_dim="ProductTypeId",
    )
    
    update_dim(
        data_df=pays,
        dim="DimProduct",
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )

    map_dim(
        dim="DimProduct",
        data_df=pays,
        cat_column_df="ProductId",
        cat_column_dim="ProductName",
        id_column_dim="ProductId",
    )
    
    update_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )

    map_dim(
        data_df=pays,
        dim="DimTransactionType",
        cat_column_df="TransactionTypeId",
        cat_column_dim="TransactionTypeName",
        id_column_dim="TransactionTypeId",
    )
    
    map_dim(
        dim="DimCountry",
        data_df=pays,
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )

    map_dim(
        dim="DimCountry",
        data_df=pays,
        cat_column_df="CurrencyId",
        cat_column_dim="CurrencyCode",
        id_column_dim="CurrencyId",
    )
    pays["CurrencyId"] = pays["CurrencyId"].astype(int)

    map_dim(
        dim="DimPlatform",
        data_df=pays,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )

    map_dim(
        dim="DimApp",
        data_df=pays,
        cat_column_df="AppInternalId",
        cat_column_dim="AppName",
        id_column_dim="AppInternalId",
    )

    pays["UsdGross"] = pays["UsdGross"].astype(float)
    pays["TransactionId"] = pays["TransactionId"].astype(float)
    pays["Time"] = pd.to_datetime(pays.Time)
    
    appsflyer_payments.pays_size = len(pays)

    df_to_bq(
        pays,
        table="FactTransaction",
        schema=[
            bigquery.SchemaField("Time", "TIMESTAMP"),
            bigquery.SchemaField("EventId", "INTEGER"),
            bigquery.SchemaField("TransactionId", "INTEGER"),
            bigquery.SchemaField("UsdGross", "FLOAT"),
            bigquery.SchemaField("CurrencyId", "INTEGER"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("UserAppsflyerId", "STRING"),
            bigquery.SchemaField("IDFA", "STRING"),
            bigquery.SchemaField("IDFV", "STRING"),
            bigquery.SchemaField("UserClientId", "INTEGER"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("AppInternalId", "INTEGER"),
            bigquery.SchemaField("SubscriptionDurationId", "INTEGER"),
            bigquery.SchemaField("TrialDuration", "FLOAT"),
            bigquery.SchemaField("RebillPeriod", "INTEGER"),
            bigquery.SchemaField("ProductTypeId", "INTEGER"),
            bigquery.SchemaField("ProductId", "INTEGER"),
            bigquery.SchemaField("TransactionTypeId", "INTEGER"),
        ],
        overwrite=False,
    )
