def ironsrc_revenue():
    
    from google.cloud import bigquery, storage
    import requests
    import json
    from tqdm import tqdm
    import pandas as pd
    import time
    import logging
    from datetime import datetime, timedelta
    import numpy as np

    from helpers.data_processing import get_billing_events
    from helpers.dimensions import form_dim, update_dim, map_dim
    from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql, dml_for_bq
    from clients.ironsource import Ironsrc

    secretkey = "6dc299817f5836174e69d9a5e26d9fca"
    refreshToken = "9691bb13dd7646d59ec7bd6fa1f58ac6"

    irnsrc = Ironsrc(secretkey, refreshToken)

    date_from = (datetime.now().date() - timedelta(days=1)).isoformat()
    date_to = (datetime.now().date() - timedelta(days=0)).isoformat()

    adv_report = irnsrc.report(date_from=date_from, date_to=date_to)
    
    adv_report.drop(columns=["bundleId", "appKey"], inplace=True)

    adv_report.rename(
        columns={
            "appName": "AppId",
            "countryCode": "CountryId",
            "date": "Date",
            "providerName": "InAppAdNetworkId",
            "adUnits": "InAppAdTypeId",
            "platform": "PlatformId",
           # "segment": "InAppAdSegmentId",
           # "placement": "InAppAdPlacementId",
            "adSourceChecks": "Requests",
           # "fills": "Fills",
            "adSourceAvailabilityRate": "FillRate",
            "impressions": "Impressions",
            "clicks": "Clicks",
            "clickThroughRate": "CTR",
            "videoCompletions": "Views",
            "revenue": "Revenue",
            "ecpm": "eCPM",
        },
        inplace=True,
    )
    
    adv_report['InAppAdSegmentId'] = 'Unknown'
    adv_report['InAppAdPlacementId'] = 'Unknown'
    adv_report['Fills'] = (adv_report['Requests'] * (adv_report['FillRate']/100)).astype(int)
    
    adv_report["AppId"] = adv_report["AppId"].map(
        {
            "Nebula: Horoscope & Astrology (Android)": 0,
            "Nebula: Horoscope & Astrology (iOS)": 0,
            "Zodi: Horoscope & Astrology (iOS)": 4,
            'Party Game! (iOS)': 5
        }
    )

    adv_report["PlatformId"] = adv_report["PlatformId"].apply(
        lambda x: "ios" if x == "iOS" else 'android'
    )

    adv_report["CountryId"] = adv_report["CountryId"].replace(["ZZ", "NA",'XX'], "Unknown")
    
    object_columns = [
        "AppId",
        "InAppAdNetworkId",
        "InAppAdTypeId",
        "PlatformId",
        "InAppAdSegmentId",
        "InAppAdPlacementId",
        "CountryId",
    ]

    for col in object_columns:
        adv_report[col] = adv_report[col].replace([0, -1, "None", np.nan], "Unknown")
        
    numeric_columns = [
        "Requests",
        "Fills",
        "Impressions",
        "FillRate",
        "Clicks",
        "CTR",
        "Views",
        "Revenue",
        "eCPM",
    ]

    decimal_columns = [
        "FillRate",
        "CTR",
        "Revenue",
        "eCPM",
    ]

    for col in numeric_columns:
        adv_report[col] = adv_report[col].replace([0, -1, "None", np.nan, "Unknown"], 0)

    for col in decimal_columns:
        adv_report[col] = adv_report[col].round(2)

    update_dim(
        data_df=adv_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    update_dim(
        data_df=adv_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )
    
    update_dim(
        data_df=adv_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )

    update_dim(
        data_df=adv_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )
    
    update_dim(
        dim="DimPlatform",
        data_df=adv_report,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )

    map_dim(
        dim="DimApp",
        data_df=adv_report,
        cat_column_df="AppId",
        cat_column_dim="AppName",
        id_column_dim="AppInternalId",
    )

    map_dim(
        dim="DimCountry",
        data_df=adv_report,
        cat_column_df="CountryId",
        cat_column_dim="CountryCodeShort",
        id_column_dim="CountryId",
    )
    
    map_dim(
        dim="DimPlatform",
        data_df=adv_report,
        cat_column_df="PlatformId",
        cat_column_dim="PlatformName",
        id_column_dim="PlatformId",
    )
    
    map_dim(
        data_df=adv_report,
        dim="DimInAppAdNetwork",
        cat_column_df="InAppAdNetworkId",
        cat_column_dim="InAppAdNetworkName",
        id_column_dim="InAppAdNetworkId",
    )

    map_dim(
        data_df=adv_report,
        dim="DimInAppAdType",
        cat_column_df="InAppAdTypeId",
        cat_column_dim="InAppAdTypeName",
        id_column_dim="InAppAdTypeId",
    )
    
    map_dim(
        data_df=adv_report,
        dim="DimInAppAdSegment",
        cat_column_df="InAppAdSegmentId",
        cat_column_dim="InAppAdSegmentName",
        id_column_dim="InAppAdSegmentId",
    )
    
    map_dim(
        data_df=adv_report,
        dim="DimInAppAdPlacement",
        cat_column_df="InAppAdPlacementId",
        cat_column_dim="InAppAdPlacementName",
        id_column_dim="InAppAdPlacementId",
    )
    
    adv_report["Date"] = pd.to_datetime(adv_report["Date"])

    ironsrc_revenue.report_size = len(adv_report)
    
    ironsrc_revenue.target_table = 'FactAdRevenue'

    df_to_bq(
        adv_report,
        table=ironsrc_revenue.target_table,
        schema=[
            bigquery.SchemaField("Date", "Date"),
            bigquery.SchemaField("AppId", "INTEGER"),
            bigquery.SchemaField("CountryId", "INTEGER"),
            bigquery.SchemaField("InAppAdNetworkId", "INTEGER"),
            bigquery.SchemaField("InAppAdTypeId", "INTEGER"),
            bigquery.SchemaField("PlatformId", "INTEGER"),
            bigquery.SchemaField("InAppAdSegmentId", "INTEGER"),
            bigquery.SchemaField("InAppAdPlacementId", "INTEGER"),
            bigquery.SchemaField("Requests", "INTEGER"),
            bigquery.SchemaField("Fills", "INTEGER"),
            bigquery.SchemaField("Impressions", "INTEGER"),
            bigquery.SchemaField("FillRate", "FLOAT"),
            bigquery.SchemaField("Clicks", "INTEGER"),
            bigquery.SchemaField("CTR", "FLOAT"),
            bigquery.SchemaField("Views", "INTEGER"),
            bigquery.SchemaField("Revenue", "FLOAT"),
            bigquery.SchemaField("eCPM", "FLOAT"),
        ],
        overwrite=False,
    ) 