import os
import time
from datetime import datetime, timedelta
from time import gmtime, strftime
import dask.dataframe as dd
import gcsfs
import pandas as pd
from dask.diagnostics import ProgressBar
from google.cloud import bigquery, storage
from tqdm import tqdm
import pymysql
from sqlalchemy import create_engine

def df_from_bq(query: str, table=None, compute=True, output=True, dtype=None):
    
    global gs_path, gcs
    project = "nebula-horoscope"
    key = "key.json"
    gcs = gcsfs.GCSFileSystem(project=project, token=key, cache_timeout=0)
    bq_client = bigquery.Client.from_service_account_json(key)
    storage_client = storage.Client.from_service_account_json(key)

    prefix = "gs://"
    dataset = "Analytics"
    table_name = (
        "result_" + str(strftime("%Y%m%d_%H%M%S", gmtime())) if table == None else table
    )
    destination_file = "/" + table_name + "_*" + ".csv"
    gs_bucket = "bq-etl-nebula"
    gs_folder = "Pipeline-Temp"
    gs_path = (
        prefix + gs_bucket if gs_folder == "" else prefix + gs_bucket + "/" + gs_folder
    )

    job_config = bigquery.QueryJobConfig()
    table_ref = bq_client.dataset(dataset).table(table_name)
    job_config.destination = table_ref
    job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE

    query_job = bq_client.query(query, location="US", job_config=job_config)
    query_job.result()

    if output == True:
        print("Query results loaded to table {0}".format(table_ref.path))
    else:
        pass

    destination_uri = gs_path + "{0}".format(destination_file)
    dataset_ref = bq_client.dataset(dataset, project=project)
    table_ref = dataset_ref.table(table_name)

    extract_job = bq_client.extract_table(table_ref, destination_uri, location="US")
    extract_job.result()

    if output == True:
        print("Query results extracted to GCS: {0}".format(destination_uri))
    else:
        pass

    bq_client.delete_table(table_ref)

    if output == True:
        print("Table {0} deleted from {1}".format(table_name, dataset))
    else:
        pass

    gcs.clear_instance_cache()
    df = dd.read_csv(
        gs_path + "{0}".format(destination_file),
        storage_options={"token": key},
        dtype=dtype,
        low_memory=False,
    )

    if compute == True and output == True:
        with ProgressBar():
            print("Computing:")
            df = df.compute().reset_index(drop=True)
            print("Results imported to DD!")
    elif output == False and compute == True:
        df = df.compute().reset_index(drop=True)
    else:
        pass

    gcs.invalidate_cache()
    for i in gcs.glob(gs_path + "/"):
        if table_name in i:
            gcs.rm(i)

            if output == True:
                print("Result file {} deleted".format(table_name + ".csv"))
            else:
                pass

    return df


def df_from_mysql(query: str, port=3309, host=None, schema=None, output=True):

    user = "analyticsuser"
    pswd = "TerofiNffjr**3jfj#nNvkkqghg"
    if host == None:
        host = "bs-prod.infra.nebulahoroscope.com" 
    port = str(port)
    if schema == None:
        schema = 'nebula'

    mysql_conn_str = "mysql+pymysql://{0}:{1}@{2}:{3}/{4}".format(
        user, pswd, host, port, schema
    )
    mysql_conn = create_engine(mysql_conn_str, pool_recycle=1)

    if output:
        print("Connected to {}:{}".format(host, port))
    else:
        pass
    
    df = pd.read_sql(query, con=mysql_conn)
    
    if output:
        print("Data extracted to DataFrame!")
    else:
        pass    

    return df


# def df_to_bq(
#     df, dataset="DWH", table=None, schema=None, overwrite=False, partition_column=None
# ):  
#     key = "key.json" 
#     table_name = "test" if table == None else table
#     bq_client = bigquery.Client.from_service_account_json(key)
#     table_ref = bq_client.dataset(dataset).table("{}".format(table_name))

#     job_config = bigquery.job.LoadJobConfig()
#     job_config.destination = table_ref
#     job_config.autodetect = True
#     job_config.ignore_unknown_values = True
#     if schema != None:
#         job_config.schema = schema
#     else:
#         pass

#     if overwrite == True:
#         job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
#     else:
#         job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND

#     job = bq_client.load_table_from_dataframe(
#         df, destination=table_ref, location="US", job_config=job_config
#     )

#     job.result()

#     assert job.state == "DONE"
#     table = bq_client.get_table(table_ref)

#     if overwrite == True:
#         print("Data overwritten in {}".format(table_ref))
#     else:
#         print("Data loaded to {}".format(table_ref))
        
def df_to_bq(df, dataset="DWH", table=None, schema=None, overwrite=False, stream=False):

    def df_to_bq_load(
        df, dataset="DWH", table=None, schema=None, overwrite=False 
    ):  
        key = "key.json" 
        table_name = "test" if table == None else table
        bq_client = bigquery.Client.from_service_account_json(key)
        table_ref = bq_client.dataset(dataset).table("{}".format(table_name))

        job_config = bigquery.job.LoadJobConfig()
        job_config.destination = table_ref
        job_config.autodetect = True
        job_config.ignore_unknown_values = True
        if schema != None:
            job_config.schema = schema
        else:
            pass

        if overwrite == True:
            job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
        else:
            job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND

        job = bq_client.load_table_from_dataframe(
            df, destination=table_ref, location="US", job_config=job_config
        )

        job.result()

        assert job.state == "DONE"
        table = bq_client.get_table(table_ref)

        if overwrite == True:
            print("Data overwritten in {}".format(table_ref))
        else:
            print("Data loaded to {}".format(table_ref))

    def df_to_bq_stream( 
        df, dataset="DWH", table=None, schema=None, overwrite=False 
    ):  
        key = "key.json" 
        table_name = "test" if table == None and overwrite == True else table
        bq_client = bigquery.Client.from_service_account_json(key)
        table_ref = bq_client.dataset(dataset).table("{}".format(table_name))

        if schema == None and overwrite == True:
            raise NotImplementedError("The destination table has no schema.")
        else:
            pass

        if overwrite == True:
            bq_client.delete_table(table_ref, not_found_ok=True)
            table = bigquery.Table(table_ref, schema=schema)
            bq_client.create_table(table)
            table = bq_client.get_table(table_ref)
        else:
            table = bq_client.get_table(table_ref)

        job = bq_client.insert_rows_from_dataframe(table, df)
        rows_after = bq_client.get_table(table_ref).num_rows

        if overwrite == True:
            print("Data overwritten in {}".format(table_ref))
        else:
            print("Data loaded to {}".format(table_ref))
    
    if stream==False:
        df_to_bq_load(df, dataset=dataset, table=table, schema=schema, overwrite=overwrite)
    else:
        df_to_bq_stream(df, dataset=dataset, table=table, schema=schema, overwrite=overwrite)
        
    
def dml_for_bq(query, output=True):
    key = "key.json" 
    bq_client = bigquery.Client.from_service_account_json(key)
    query_job = bq_client.query(
                    query,
                    location='US')
    query_job.result()
    
    if output:
        print('{} job done!'.format(query))
        
def bq_to_gcs(
    project,
    dataset_id,
    gs_bucket,
    gs_folder,
    table,
    destination_format="json",
    compression="GZIP",
    delete_bq_table=False,
    ):

    from google.cloud import bigquery, storage
    from time import strftime, gmtime

    allowed_formats = ["json", "avro", "csv"]

    if destination_format not in allowed_formats:
        raise ValueError(
            "{} is not allowed. Please choose either of {}".format(
                destination_format, allowed_formats
            )
        )

    key = "key.json"
    bq_client = bigquery.Client.from_service_account_json(key)
    prefix = "gs://"

    destination_extension = "." + destination_format
    table_name = table.table_id
    destination_table_name = table_name + str(strftime("%Y%m%d_%H%M%S", gmtime()))
    destination_file = "/" + destination_table_name + "_*" + destination_extension
    gs_subfolder = table_name 
    gs_path = prefix + gs_bucket + "/" + gs_folder + "/" + gs_subfolder

    destination_uri = gs_path + "{0}".format(destination_file)
    dataset_ref = bq_client.dataset(dataset_id, project=project)
    table_ref = dataset_ref.table(table_name)

    job_config = bigquery.job.ExtractJobConfig()

    job_config.compression = compression.upper()
    job_config.destination_format = (
        destination_format.upper()
        if destination_format != "json"
        else "NEWLINE_DELIMITED_JSON"
    )

    extract_job = bq_client.extract_table(
        table_ref, destination_uri, job_config=job_config, location="US"
    )
    extract_job.result()
    if delete_bq_table == True:
        bq_client.delete_table(table_ref)

    print(table.table_id + " saved to " + gs_path)

    bq_client.close()