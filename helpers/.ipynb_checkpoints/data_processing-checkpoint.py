import numpy as np
import pandas as pd

def get_billing_events():
    
    global events
    events = []
    event_period = {}
    event_duration={}
    event_type={}
    sub_type = ["wk", "mo", "3mo", "6mo", "yr", "lifetime"]
    periods = np.arange(1, 150, 1)
    for st in sub_type:
        for p in periods:
            if st == "lifetime" and p > 6:
                pass
            elif st == "yr" and p > 6:
                pass
            elif st == "6mo" and p > 6:
                pass
            elif st == "3mo" and p > 12:
                pass
            elif st == "mo" and p > 24:
                pass
            else:
                events.append(str("billing_success_" + str(p) + "_" + st))
                event_period[str("billing_success_" + str(p) + "_" + st)] = p
                event_duration[str("billing_success_" + str(p) + "_" + st)] = st
                event_type[str("billing_success_" + str(p) + "_" + st)] = 'regular'

    events = events + ["iap_purchase_success", "subscription_wk_start_success", "back_billing_refund_subscription"]
    events = ",".join(events)
    
    event_period["subscription_wk_start_success"] = 0
    event_duration["subscription_wk_start_success"] = "wk"
    
    event_type["subscription_wk_start_success"] = "trial"
    event_type["back_billing_refund_subscription"] = "refund"
    event_type["iap_purchase_success"] = "regular"
    
    
    return events, event_period, event_duration, event_type
    
def lookup_dt(s):
    dates = {date: pd.to_datetime(date) for date in s.unique()}
    return s.map(dates)

