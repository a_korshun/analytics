import os
import time
from datetime import datetime, timedelta
from time import gmtime, strftime
import numpy as np
import pandas as pd
from tqdm import tqdm
from helpers.data_etl import df_from_bq, df_to_bq, df_from_mysql


def form_dim(
    data_df, dim, cat_column_df, id_column_dim, cat_column_dim, additional_columns=[]
):

    df = data_df[(data_df[cat_column_df] != "Unknown") & (data_df[cat_column_df] != -1)]

    columns = additional_columns + [cat_column_df]

    dimension = pd.DataFrame(df[columns]).drop_duplicates().reset_index(drop=True)
    dimension.rename(columns={cat_column_df: cat_column_dim}, inplace=True)

    dimension[cat_column_dim] = dimension[cat_column_dim].astype("category")
    dimension[id_column_dim] = dimension[cat_column_dim].values.codes

    columns = [id_column_dim, cat_column_dim] + additional_columns

    dimension = dimension[columns]

    dimension.loc[dimension.shape[0]] = [-1, "Unknown"] + [
        'Unknown' for i in range(len(additional_columns))
    ]

    for c in additional_columns:
        dimension[c] = dimension[c].astype(
            str
        )  # .replace(r'[a-zA-Z%]', -1, regex=True, inplace=True)

    df_to_bq(dimension, dataset="DWH", table=dim, overwrite=True)

    print("{} created!".format(dim))
    
    
def update_dim(
    data_df, dim, cat_column_dim, cat_column_df, id_column_dim, additional_columns=[]
):

    dim_df = df_from_bq("""select * from DWH.{}""".format(dim), output=False)

    unique_df_cats = pd.Series(data_df[cat_column_df].unique())
    unique_dim_cats = pd.Series(dim_df[cat_column_dim].unique())

    if not (unique_df_cats.isin(unique_dim_cats).all()):

        print("New pairs found in {}!".format(dim))

        new_names = unique_df_cats[~unique_df_cats.isin(unique_dim_cats)].unique()

        max_idx_current = dim_df[id_column_dim].max() + 1
        max_idx_needed = dim_df[id_column_dim].max() + len(new_names) + 1

        upd = {
            cat_column_dim: new_names,
            id_column_dim: np.arange(max_idx_current, max_idx_needed).tolist(),
        }

        upd_df = pd.DataFrame(upd)

        columns = additional_columns + [cat_column_df]

        add_cols_df = data_df[data_df[cat_column_df].isin(new_names)][
            columns
        ].drop_duplicates()
        add_cols_df.rename(columns={cat_column_df: cat_column_dim}, inplace=True)

        for c in additional_columns:
            add_cols_df[c], dim_df[c] = (
                add_cols_df[c].astype(str),
                dim_df[c].astype(str),
            )

        if len(additional_columns) > 0:
            upd_df = upd_df.merge(add_cols_df, on=cat_column_dim)

        dim_df = dim_df.append(upd_df).reset_index(drop=True)

        df_to_bq(dim_df, dataset="DWH", table=dim, overwrite=True)

        rows = add_cols_df.shape[0]

        print("{0} updated. {1} pairs added!".format(dim, rows))

    else:
        print("Current {} is relevant!".format(dim))
        
        
def map_dim(dim, data_df, cat_column_df, cat_column_dim, id_column_dim):

    dim_df = df_from_bq(
        """select {0}, {1} from DWH.{2}""".format(cat_column_dim, id_column_dim, dim),
        output=False,
    )

    mapping = dict(zip(dim_df[cat_column_dim], dim_df[id_column_dim]))

    data_df[cat_column_df] = data_df[cat_column_df].map(mapping)

    print("IDs mapped for {}!".format(dim))