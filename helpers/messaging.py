import slack 
import json

def msg_to_slack_me(message: str, to="analytics_alarm", attachment=None):
    slack_token = "xoxb-8482770630-1292561647698-YmUqoldfnKwffFQsiRQWNJY5"
    sc = slack.WebClient(token=slack_token)
    attachment = json.dumps(attachment) if attachment != None else None
    response = sc.chat_postMessage(channel=to, text=message, attachments=attachment)
    print("Sent to {}!".format(to))
    
def format_slack_attachment(func_name, target, message):
    attachment_json = [
        {
            "color": "#ccffcc",
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*{}*".format(func_name)
                    },
                },
                {
                    "type": "section",
                    "fields": [
                        {
                            "type": "mrkdwn",
                            "text": "{}".format(message)
                        },
                        {
                            "type": "mrkdwn",
                            "text": "*Target:*\n{}".format(target)
                        },
                    ],
                },
            ],
        }
    ]
    return attachment_json